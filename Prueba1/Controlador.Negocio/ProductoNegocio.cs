﻿
using Prueba1.Modelo.Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba1.Controlador.Negocio
{

    
   
    class ProductoNegocio
    {

        private ProductoDatos prod = new ProductoDatos();


        public DataTable MostrarProduc() {
            DataTable table;
            table = prod.Mostrar();
            return table;
       
        }

        public void InsertarProducto(String Descripcion, String Precio, String Stock, String Stockcritico, String Fkproveedor, String Fktipoproduct, String dia, String mes, String anno) {
            ProductoDatos pro = new ProductoDatos();
            pro.Descripcion = Descripcion;
            pro.Precio = Int32.Parse(Precio);
            pro.Stock = Int32.Parse(Stock);
            pro.Stockcritico = Int32.Parse(Stockcritico);
            pro.Fkproveedor = Int32.Parse(Fkproveedor);
            pro.Fktipoproduct = Int32.Parse(Fktipoproduct);
            int dia1 = Int32.Parse(dia);
            int mes1 = Int32.Parse(mes);
            int anno1 = Int32.Parse(anno);
            prod.Insertar(pro, dia1, mes1, anno1);

        
        
        }

        public void ModificarProducto(String Descripcion, String Precio, String Stock, String Stockcritico, String Fkproveedor, String Fktipoproduct, String dia, String mes, String anno,String idx) {

            ProductoDatos pro = new ProductoDatos();
            pro.Idx = Int64.Parse(idx);
            pro.Descripcion = Descripcion;
            pro.Precio = Int32.Parse(Precio);
            pro.Stock = Int32.Parse(Stock);
            pro.Stockcritico = Int32.Parse(Stockcritico);
            pro.Fkproveedor = Int32.Parse(Fkproveedor);
            pro.Fktipoproduct = Int32.Parse(Fktipoproduct);
            int dia1 = Int32.Parse(dia);
            int mes1 = Int32.Parse(mes);
            int anno1 = Int32.Parse(anno);
            prod.Modificar(pro, dia1, mes1, anno1);
        }

        public void EliminarProducto(String idx) {
            ProductoDatos pro = new ProductoDatos();
            pro.Idx = Int64.Parse(idx);
            pro.Eliminar(pro);

        }
    
    
    
    }







}
