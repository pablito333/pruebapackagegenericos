﻿using Prueba1.Modelo.Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Prueba1.Controlador.Negocio
{
    class ProveedorNegocio
    {

        private ProveedorDatos prov = new ProveedorDatos();

        public DataTable MostrarProveedor()
        {
            DataTable table;
            table = prov.Mostrar();
            return table;
           

        }

        public void InsertarProveedor(String nombre, String fono, String rubro, String user, String password) {

            ProveedorDatos proveedor = new ProveedorDatos();
            proveedor.Nombre = nombre;
            proveedor.Fono = Int64.Parse(fono);
            proveedor.Rubro = rubro;
            proveedor.User = user;
            proveedor.Password = password;
            proveedor.Insertar(proveedor);



        }
        public void ModificarProveedor(String nombre, String fono, String rubro, String user, String password, String idx)
        {

            ProveedorDatos proveedor = new ProveedorDatos();
            proveedor.Idx = Int64.Parse(idx);
            proveedor.Nombre = nombre;
            proveedor.Fono = Int64.Parse(fono);
            proveedor.Rubro = rubro;
            proveedor.User = user;
            proveedor.Password = password;
            proveedor.Modificar(proveedor);



        }
        public void EliminarProveedor(String idx)
        {
            ProveedorDatos proveedor = new ProveedorDatos();
            proveedor.Idx = Int64.Parse(idx);
            proveedor.Eliminar(proveedor);

        }

    }
}
