﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prueba1
{
    class ConexionOracle

    {
        string oradb;
        public ConexionOracle()
        {  
            this.oradb = ConfigurationManager.ConnectionStrings["conexion"].ConnectionString;
           
        }

        public OracleConnection Conectar()
        {
            OracleConnection conn = new OracleConnection();
            conn.ConnectionString = oradb;
            try 
            { 
                conn.Open();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return conn;

        }

        public OracleConnection GetOracleConnection() {
            OracleConnection conn = new OracleConnection();
            conn.ConnectionString = oradb;
            return conn;



        }






    }
}
