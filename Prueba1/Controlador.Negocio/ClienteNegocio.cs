﻿using Prueba1.Modelo.Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

namespace Prueba1.Controlador.Negocio
{
    class ClienteNegocio
    {

        private ClienteDatos client = new ClienteDatos();

        public DataTable MostrarCliente()
        {
            DataTable table;
            table = client.Mostrar();
            return table;

        }
        //INSERTAR UN CLIENTE DE TIPO PERSONA NATURAL
        public void InsertarClientePersona(String User,String Password, String Rut, String Calle, String Numerocalle,String Mail,String Telefono,String Comuna,String Nombre,String Apellido)
        {
            ClientePersona clientepersona = new ClientePersona();
            clientepersona.User = User;
            clientepersona.Password = Password;
            clientepersona.Rut = Rut;
            clientepersona.Calle = Calle;
            clientepersona.Numerocalle = Int32.Parse(Numerocalle);
            clientepersona.Mail = Mail;
            clientepersona.Telefono = Int32.Parse(Telefono);
            clientepersona.Comuna = Int32.Parse(Comuna);
            clientepersona.Nombre = Nombre;
            clientepersona.Apellido = Apellido;
            
            clientepersona.Insertar(clientepersona);

        }
        //INSERTAR UN CLIENTE DE TIPO EMPRESA
        public void InsertarClienteEmpresa(String User, String Password, String Rut, String Calle, String Numerocalle, String Mail, String Telefono, String Comuna, String Nombre, String Rol)
        {
            ClienteEmpresa clienteempresa = new ClienteEmpresa();
            clienteempresa.User = User;
            clienteempresa.Password = Password;
            clienteempresa.Rut = Rut;
            clienteempresa.Calle = Calle;
            clienteempresa.Numerocalle = Int32.Parse(Numerocalle);
            clienteempresa.Mail = Mail;
            clienteempresa.Telefono = Int32.Parse(Telefono);
            clienteempresa.Comuna = Int32.Parse(Comuna);
            clienteempresa.Nombreempresa = Nombre;
            clienteempresa.Rol = Rol;

            clienteempresa.Insertar(clienteempresa);

        }

        //ELIMINA UN CLIENTE
        public void EliminarCliente(String idx)
        {
            ClienteDatos cliente = new ClienteDatos();
            cliente.Idx = Int64.Parse(idx);
            cliente.Eliminar(cliente);

        }
        //MODIFICA CLIENTE TIPO PERSONA
        public void ModificarClientePersona(String User, String Password, String Rut, String Calle, String Numerocalle, String Mail, String Telefono, String Comuna, String Nombre, String Apellido, String idx)
        {
            ClientePersona clientepersona = new ClientePersona();
            clientepersona.Idx = Int64.Parse(idx);
            clientepersona.User = User;
            clientepersona.Password = Password;
            clientepersona.Rut = Rut;
            clientepersona.Calle = Calle;
            clientepersona.Numerocalle = Int32.Parse(Numerocalle);
            clientepersona.Mail = Mail;
            clientepersona.Telefono = Int32.Parse(Telefono);
            clientepersona.Comuna = Int32.Parse(Comuna);
            clientepersona.Nombre = Nombre;
            clientepersona.Apellido = Apellido;

            clientepersona.Modificar(clientepersona);

        }

        //MODIFICAR CLIENTE TIPO 
        public void ModificarClienteEmpresa(String User, String Password, String Rut, String Calle, String Numerocalle, String Mail, String Telefono, String Comuna, String Nombre, String Rol, String idx)
        {
            ClienteEmpresa clienteempresa = new ClienteEmpresa();
            clienteempresa.Idx = Int64.Parse(idx);
            clienteempresa.User = User;
            clienteempresa.Password = Password;
            clienteempresa.Rut = Rut;
            clienteempresa.Calle = Calle;
            clienteempresa.Numerocalle = Int32.Parse(Numerocalle);
            clienteempresa.Mail = Mail;
            clienteempresa.Telefono = Int32.Parse(Telefono);
            clienteempresa.Comuna = Int32.Parse(Comuna);
            clienteempresa.Nombreempresa = Nombre;
            clienteempresa.Rol = Rol;
            clienteempresa.Modificar(clienteempresa);

        }







    }
}
