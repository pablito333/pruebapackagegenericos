﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Prueba1.Modelo.Datos;

namespace Prueba1.Controlador.Negocio
{
    
    public class FuncionarioNegocio
    {
        FuncionarioDatos funcDatos = new FuncionarioDatos();
        public bool LoginUser(string user, string pass)
        {
            return funcDatos.Login(user, pass);
        }

        public DataTable MostrarFuncionario()
        {
            DataTable table;
            table = funcDatos.Mostrar();
            return table;

        }

        public void InsertarFuncionario(string rut,string nombre, string apellido,string user,string pass,string cargo) {

            FuncionarioDatos funcionario = new FuncionarioDatos();
            funcionario.Rut = rut;
            funcionario.Nombref = nombre;
            funcionario.Apellidof = apellido;
            funcionario.User = user;
            funcionario.Pass = pass;
            funcionario.Cargoid = Int32.Parse(cargo);
            funcionario.Insertar(funcionario);

        }

        public void ModificarFuncionario(string rut, string nombre, string apellido, string user, string pass, string cargo, string idx) {
            FuncionarioDatos funcionario = new FuncionarioDatos();
            funcionario.Rut = rut;
            funcionario.Nombref = nombre;
            funcionario.Apellidof = apellido;
            funcionario.User = user;
            funcionario.Pass = pass;
            funcionario.Cargoid = Int32.Parse(cargo);
            funcionario.Idx = idx;
            funcionario.Modificar(funcionario);
        }
        public void Eliminar(string idx) {
            FuncionarioDatos funcionario = new FuncionarioDatos();
            funcionario.Idx = idx;
            funcionario.Eliminar(funcionario);


        }

        public int ValidarRutFuncionario(string rut) {
            FuncionarioDatos funcionario = new FuncionarioDatos();
            funcionario.Rut = rut;
            return funcionario.ValidateRut(funcionario);
        }
        public int ValidarUserFuncionario(string user)
        {
            FuncionarioDatos funcionario = new FuncionarioDatos();
            funcionario.User = user;
            return funcionario.ValidateUser(funcionario);
        }





    }

}
