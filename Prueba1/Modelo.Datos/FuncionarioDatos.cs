﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Prueba1.Modelo.Datos
{
    public class FuncionarioDatos
    {
        
        private string rut;
        private string nombref;
        private string apellidof;
        private string user;
        private string pass;
        private int cargoid;
        private string idx;

        OracleDataAdapter objAdapter = new OracleDataAdapter();
        OracleDataReader rdr;
        private ConexionOracle connection = new ConexionOracle();
        OracleCommand command = new OracleCommand();
        DataTable dt = new DataTable();
        public FuncionarioDatos()
        {
            this.rut = "";
            this.nombref = "";
            this.apellidof = "";
            this.user = "";
            this.pass = "";
            this.cargoid = 0;
            this.idx = "";
        }
        public FuncionarioDatos(string rut, string nombref, string apellidof, string user, string pass, int cargoid,
            string idx)
        {
            this.rut = rut;
            this.nombref = nombref;
            this.apellidof = apellidof;
            this.user = user;
            this.pass = pass;
            this.cargoid = cargoid;
            this.idx = idx;
        }

        public string Idx { get => idx; set => idx = value; }
        public string Rut { get => rut; set => rut = value; }
        public string Nombref { get => nombref; set => nombref = value; }
        public string Apellidof { get => apellidof; set => apellidof = value; }
        public string User { get => user; set => user = value; }
        public string Pass { get => pass; set => pass = value; }
        public int Cargoid { get => cargoid; set => cargoid = value; }




        public bool Login(string user, string pass)
        {
          command.Connection = connection.Conectar(); 
          command.CommandText = "PS_LOGIN.PS_VALIDAR";
          command.CommandType = CommandType.StoredProcedure;
          command.Parameters.Add("USERX", OracleDbType.Varchar2, 50).Value = user;
          command.Parameters.Add("PASSWORDX", OracleDbType.Varchar2, 50).Value = pass;
          command.Parameters.Add("VALIDATEX", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
          try
          {
            rdr = command.ExecuteReader();
            if (rdr.HasRows)
            {
              return true;
            }
            else
            {
              return false;
            }
                                         
          }
          catch (Exception ex) { MessageBox.Show(ex.Message); }
          command.Parameters.Clear();
          return true;
        }


        public DataTable Mostrar()
        {

            command.Connection = connection.Conectar();
            command.CommandText = "ps_funcionario.cargar_funcionarios";
            command.BindByName = true;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("funcionario", OracleDbType.RefCursor).Direction = ParameterDirection.ReturnValue;
            objAdapter.SelectCommand = command;
            objAdapter.Fill(dt);
            command.Parameters.Clear();
            return dt;
        }

        public void Insertar(FuncionarioDatos funcionario)
        {
            command.Connection = connection.Conectar();
            command.CommandText = "ps_funcionario.agregar_funcionario";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("RUTX", OracleDbType.Varchar2, 50).Value = funcionario.Rut;
            command.Parameters.Add("NOMBREX", OracleDbType.Varchar2, 50).Value = funcionario.Nombref;
            command.Parameters.Add("APELLIDOX", OracleDbType.Varchar2, 50).Value = funcionario.Apellidof;
            command.Parameters.Add("USERX", OracleDbType.Varchar2, 50).Value = funcionario.User;
            command.Parameters.Add("PASSX", OracleDbType.Varchar2, 50).Value = funcionario.Pass;
            command.Parameters.Add("IDCARGO", OracleDbType.Int32).Value = funcionario.Cargoid;




            try
            {
                command.ExecuteNonQuery();
                MessageBox.Show("Funcionario Ingresado");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            command.Parameters.Clear();

        }

        public void Modificar(FuncionarioDatos funcionario) {

            command.Connection = connection.Conectar();
            command.CommandText = "ps_funcionario.modificar_funcionario";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("RUTX", OracleDbType.Varchar2, 50).Value = funcionario.Rut;
            command.Parameters.Add("NOMBREX", OracleDbType.Varchar2, 50).Value = funcionario.Nombref;
            command.Parameters.Add("APELLIDOX", OracleDbType.Varchar2, 50).Value = funcionario.Apellidof;
            command.Parameters.Add("USERX", OracleDbType.Varchar2, 50).Value = funcionario.User;
            command.Parameters.Add("PASSX", OracleDbType.Varchar2, 50).Value = funcionario.Pass;
            command.Parameters.Add("IDCARGO", OracleDbType.Int32).Value = funcionario.Cargoid;
            command.Parameters.Add("IDX", OracleDbType.Varchar2, 50).Value = funcionario.Idx;

            try
            {
                command.ExecuteNonQuery();
                MessageBox.Show("Funcionario Editado");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            command.Parameters.Clear();

        }

        public void Eliminar(FuncionarioDatos funcionario)
        {
            command.Connection = connection.Conectar();
            command.CommandText = "ps_funcionario.eliminar_funcionario";
            command.BindByName = true;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("IDX", OracleDbType.Varchar2, 20).Value = funcionario.Idx;
            try
            {
                command.ExecuteNonQuery();
                MessageBox.Show("Funcionario Eliminado");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            command.Parameters.Clear();

        }

        public int ValidateRut(FuncionarioDatos funcionario) {
            
            command.Connection = connection.Conectar();
            command.CommandText = "ps_validaciones.validar_funcionario_rut";
            command.BindByName = true;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("rut_fun", OracleDbType.Varchar2)).Value = funcionario.Rut;
            command.Parameters.Add("validator", OracleDbType.Int32).Direction = ParameterDirection.ReturnValue;
            try
            {
                command.ExecuteNonQuery();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            int resultadouser = int.Parse(command.Parameters["validator"].Value.ToString());
            command.Connection.Close();
            return resultadouser;
        }

        public int ValidateUser(FuncionarioDatos funcionario)
        {

            command.Connection = connection.Conectar();
            command.CommandText = "ps_validaciones.validar_funcionario_user";
            command.BindByName = true;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("userr_funcionario", OracleDbType.Varchar2)).Value = funcionario.User;
            command.Parameters.Add("validator", OracleDbType.Int32).Direction = ParameterDirection.ReturnValue;
            try
            {
                command.ExecuteNonQuery();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            int resultadouser = int.Parse(command.Parameters["validator"].Value.ToString());
            command.Connection.Close();
            return resultadouser;
        }










    }
        
    
}
