﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prueba1.Modelo.Datos
{
    public class ProductoDatos
    {
        private long idx;
        private string descripcion;
        private int precio;
        private int stock;
        private int stockcritico;
        private int fkproveedor;
        private int fktipoproduct;

        private ConexionOracle conect = new ConexionOracle();
        private OracleCommand comand = new OracleCommand();
        OracleDataAdapter objAdapter = new OracleDataAdapter();
        DataTable dt = new DataTable();

        public ProductoDatos() {
            this.idx = 0;
            this.descripcion = "";
            this.precio = 0;
            this.stock = 0;
            this.stockcritico = 0;
            this.fkproveedor = 0;
            this.fktipoproduct = 0;
        }

        public ProductoDatos(long idx,string descripcion, int precio, int stock, int stockcritico, int fkfamilia, int fkproveedor, int fktipoproduct)
        {
            this.idx = idx;
            this.descripcion = descripcion;
            this.precio = precio;
            this.stock = stock;
            this.stockcritico = stockcritico;
            this.fkproveedor = fkproveedor;
            this.fktipoproduct = fktipoproduct;
        }

       

        public string Descripcion { get => descripcion; set => descripcion = value; }
        public int Precio { get => precio; set => precio = value; }
        public int Stock { get => stock; set => stock = value; }
        public int Stockcritico { get => stockcritico; set => stockcritico = value; }
  
        public int Fkproveedor { get => fkproveedor; set => fkproveedor = value; }
        public int Fktipoproduct { get => fktipoproduct; set => fktipoproduct = value; }
        public long Idx { get => idx; set => idx = value; }

        public void Insertar(ProductoDatos producto,int dia,int mes,int anno) {
            comand.Connection = conect.Conectar();
            comand.CommandText = "ps_productos.agregar_producto";
            comand.CommandType = CommandType.StoredProcedure;
            comand.Parameters.Add("DESCRIPCIONX", OracleDbType.Varchar2, 100).Value = producto.Descripcion;
            comand.Parameters.Add("PRECIOX", OracleDbType.Int32, 10).Value = producto.Precio;
            comand.Parameters.Add("STOCKX", OracleDbType.Int32, 10).Value = producto.Stock;
            comand.Parameters.Add("STOCK_CRITICOX", OracleDbType.Int32, 10).Value = producto.Stockcritico;
            comand.Parameters.Add("FK_ID_PROVEEDORX", OracleDbType.Int32, 10).Value = producto.Fkproveedor;
            comand.Parameters.Add("FK_ID_PRODUCTX", OracleDbType.Int32, 4).Value = producto.Fktipoproduct;
            comand.Parameters.Add("DIA", OracleDbType.Int32, 2).Value = dia;
            comand.Parameters.Add("MES", OracleDbType.Int32, 2).Value = mes;
            comand.Parameters.Add("ANNO", OracleDbType.Int32, 4).Value = anno;

            try { comand.ExecuteNonQuery();
                MessageBox.Show("Producto Ingresado");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            comand.Parameters.Clear();

        }
        public void Modificar(ProductoDatos producto, int dia, int mes, int anno) {
            comand.Connection = conect.Conectar();
            comand.CommandText = "ps_productos.modificar_producto";
            comand.CommandType = CommandType.StoredProcedure;
            comand.Parameters.Add("DESCRIPCIONX", OracleDbType.Varchar2, 100).Value = producto.Descripcion;
            comand.Parameters.Add("PRECIOX", OracleDbType.Int32, 10).Value = producto.Precio;
            comand.Parameters.Add("STOCKX", OracleDbType.Int32, 10).Value = producto.Stock;
            comand.Parameters.Add("STOCK_CRITICOX", OracleDbType.Int32, 10).Value = producto.Stockcritico;
            comand.Parameters.Add("FK_ID_PROVEEDORX", OracleDbType.Int32, 10).Value = producto.Fkproveedor;
            comand.Parameters.Add("FK_ID_PRODUCTX", OracleDbType.Int32, 4).Value = producto.Fktipoproduct;
            comand.Parameters.Add("DIA", OracleDbType.Int32, 2).Value = dia;
            comand.Parameters.Add("MES", OracleDbType.Int32, 2).Value = mes;
            comand.Parameters.Add("ANNO", OracleDbType.Int32, 4).Value = anno;
            comand.Parameters.Add("IDX", OracleDbType.Int64, 17).Value = producto.idx;
            try
            {
                comand.ExecuteNonQuery();
                MessageBox.Show("Producto Editado");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            comand.Parameters.Clear();


        }
        public DataTable Mostrar()
        {

            comand.Connection = conect.Conectar();
            comand.CommandText = "ps_productos.cargar_productos";
            comand.BindByName = true;
            comand.CommandType = CommandType.StoredProcedure;
            comand.Parameters.Add("producto", OracleDbType.RefCursor).Direction = ParameterDirection.ReturnValue;
            objAdapter.SelectCommand = comand;
            objAdapter.Fill(dt);
            comand.Parameters.Clear();
            return dt;
        }

        public void Eliminar(ProductoDatos producto) {
            comand.Connection = conect.Conectar();
            comand.CommandText = "ps_productos.eliminar_producto";
            comand.BindByName = true;
            comand.CommandType = CommandType.StoredProcedure;
            comand.Parameters.Add("IDX", OracleDbType.Int64, 17).Value = producto.idx;
            try
            {
                comand.ExecuteNonQuery();
                MessageBox.Show("Producto Eliminado");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            comand.Parameters.Clear();

        }



    }

}

       





