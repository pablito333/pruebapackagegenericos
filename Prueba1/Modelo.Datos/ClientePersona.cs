﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace Prueba1.Modelo.Datos
{
    public class ClientePersona:ClienteDatos
    {
        private string rut;
        private string nombre;
        private string apellido;
        private string nacimiento;

        private ConexionOracle conect = new ConexionOracle();
        private OracleCommand comand = new OracleCommand();
       
       
        public ClientePersona(long idx, string user, string password, string calle, int numerocalle, string mail, int telefono, int comuna, string rut, string nombre, string apellido, string nacimiento)
            : base( idx,  user,  password,  calle,  numerocalle, mail,  telefono, comuna)
        {
            this.Rut = rut;
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Nacimiento = nacimiento;
        }
        public ClientePersona()
            : base()
        {
            this.Rut = " ";
            this.Nombre = " ";
            this.Apellido = " ";
            this.Nacimiento = " ";
            
        }

        public string Rut { get => rut; set => rut = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public string Nacimiento { get => nacimiento; set => nacimiento = value; }

        public void Insertar(ClientePersona clientepersona) {


            comand.Connection = conect.Conectar();
            comand.CommandText = "ps_cliente.agregar_clientepersona";
            comand.CommandType = CommandType.StoredProcedure;
            comand.Parameters.Add("USUARIO", OracleDbType.Varchar2).Value = clientepersona.User;
            comand.Parameters.Add("CONTRA", OracleDbType.Varchar2).Value = clientepersona.Password;
            comand.Parameters.Add("RUTCOMPLETO", OracleDbType.Varchar2).Value = clientepersona.Rut;
            comand.Parameters.Add("DIRECCIONX", OracleDbType.Varchar2).Value = clientepersona.Calle;
            comand.Parameters.Add("NUMEROX", OracleDbType.Int32).Value = clientepersona.Numerocalle;
            comand.Parameters.Add("MAILX", OracleDbType.Varchar2).Value = clientepersona.Mail;
            comand.Parameters.Add("TELEFONOX", OracleDbType.Int32).Value = clientepersona.Telefono;
            comand.Parameters.Add("IDCOMUNAX", OracleDbType.Int32).Value = clientepersona.Comuna;
            comand.Parameters.Add("NOMBREX", OracleDbType.Varchar2).Value = clientepersona.Nombre;
            comand.Parameters.Add("APELLIDOX", OracleDbType.Varchar2).Value = clientepersona.Apellido;

            try
            {
                comand.ExecuteNonQuery();
                MessageBox.Show("Cliente Persona Natural Ingresado");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            comand.Parameters.Clear();

        }


        public void Modificar(ClientePersona clientepersona)
        {

            comand.Connection = conect.Conectar();
            comand.CommandText = "ps_cliente.modificar_clientepersona";
            comand.CommandType = CommandType.StoredProcedure;
            comand.Parameters.Add("USUARIO", OracleDbType.Varchar2).Value = clientepersona.User;
            comand.Parameters.Add("CONTRA", OracleDbType.Varchar2).Value = clientepersona.Password;
            comand.Parameters.Add("RUTCOMPLETO", OracleDbType.Varchar2).Value = clientepersona.Rut;
            comand.Parameters.Add("DIRECCIONX", OracleDbType.Varchar2).Value = clientepersona.Calle;
            comand.Parameters.Add("NUMEROX", OracleDbType.Int32).Value = clientepersona.Numerocalle;
            comand.Parameters.Add("MAILX", OracleDbType.Varchar2).Value = clientepersona.Mail;
            comand.Parameters.Add("TELEFONOX", OracleDbType.Int32).Value = clientepersona.Telefono;
            comand.Parameters.Add("IDCOMUNAX", OracleDbType.Int32).Value = clientepersona.Comuna;
            comand.Parameters.Add("NOMBREX", OracleDbType.Varchar2).Value = clientepersona.Nombre;
            comand.Parameters.Add("APELLIDOX", OracleDbType.Varchar2).Value = clientepersona.Apellido;
            comand.Parameters.Add("IDX", OracleDbType.Int64).Value = clientepersona.Idx;




            try
            {
                comand.ExecuteNonQuery();
                MessageBox.Show("Cliente Persona Natural Editado");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            comand.Parameters.Clear();

        }



    }
}
