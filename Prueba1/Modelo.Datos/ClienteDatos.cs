﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using System.Data;
using System.Windows.Forms;

namespace Prueba1.Modelo.Datos
{
    public class ClienteDatos
    {
        private long idx;
        private string user;
        private string password;
        private string calle;
        private int numerocalle;
        private string mail;
        private int telefono;
        private int comuna;

        private ConexionOracle conect = new ConexionOracle();
        private OracleCommand comand = new OracleCommand();
        OracleDataAdapter objAdapter = new OracleDataAdapter();
        DataTable dt = new DataTable();

        public long Idx { get => idx; set => idx = value; }
        public string User { get => user; set => user = value; }
        public string Password { get => password; set => password = value; }
        public string Calle { get => calle; set => calle = value; }
        public int Numerocalle { get => numerocalle; set => numerocalle = value; }
        public string Mail { get => mail; set => mail = value; }
        public int Telefono { get => telefono; set => telefono = value; }
        public int Comuna { get => comuna; set => comuna = value; }

        public ClienteDatos(long idx, string user, string password, string calle, int numerocalle, string mail, int telefono, int comuna)
        {
            this.Idx = idx;
            this.User = user;
            this.Password = password;
            this.Calle = calle;
            this.Numerocalle = numerocalle;
            this.Mail = mail;
            this.Telefono = telefono;
            this.Comuna = comuna;
        }

        public ClienteDatos()
        {
            this.Idx = 0;
            this.User = "";
            this.Password = "";
            this.Calle = "";
            this.Numerocalle = 0;
            this.Mail = "";
            this.Telefono = 0;
            this.Comuna = 0;
        }

        public DataTable Mostrar() {
            comand.Connection = conect.Conectar();
            comand.CommandText = "ps_cliente.cargar_clientes";
            comand.BindByName = true;
            comand.CommandType = CommandType.StoredProcedure;
            comand.Parameters.Add("cliente", OracleDbType.RefCursor).Direction = ParameterDirection.ReturnValue;
            objAdapter.SelectCommand = comand;
            objAdapter.Fill(dt);
            comand.Parameters.Clear();
            return dt;

            

        }

        public void Eliminar(ClienteDatos cliente)
        {
            comand.Connection = conect.Conectar();
            comand.CommandText = "ps_cliente.eliminar_cliente";
            comand.BindByName = true;
            comand.CommandType = CommandType.StoredProcedure;
            comand.Parameters.Add("IDX", OracleDbType.Int64, 17).Value = cliente.idx;
            try
            {
                comand.ExecuteNonQuery();
                MessageBox.Show("Cliente Eliminado");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            comand.Parameters.Clear();

        }


    }
}
