﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using System.Data;
using System.Windows.Forms;

namespace Prueba1.Modelo.Datos
{
    class ClienteEmpresa:ClienteDatos
    {

        private string rut;
        private string nombreempresa;
        private string rol;
        

        private ConexionOracle conect = new ConexionOracle();
        private OracleCommand comand = new OracleCommand();

        public string Rut { get => rut; set => rut = value; }
        public string Nombreempresa { get => nombreempresa; set => nombreempresa = value; }
        public string Rol { get => rol; set => rol = value; }

        public ClienteEmpresa(long idx, string user, string password, string calle, int numerocalle, string mail, int telefono, int comuna, string rut, string nombreempresa, string rol)
            : base(idx, user, password, calle, numerocalle, mail, telefono, comuna)
        {
            this.Rut = rut;
            this.Nombreempresa = nombreempresa;
            this.Rol = rol;
        }

        public ClienteEmpresa()
            : base()
        {

            this.Rut = "";
            this.Nombreempresa = "";
            this.Rol = "";


        }

        public void Insertar(ClienteEmpresa clienteempresa)
        {
            comand.Connection = conect.Conectar();
            comand.CommandText = "ps_cliente.agregar_clienteempresa";
            comand.CommandType = CommandType.StoredProcedure;
            comand.Parameters.Add("USUARIO", OracleDbType.Varchar2).Value = clienteempresa.User;
            comand.Parameters.Add("CONTRA", OracleDbType.Varchar2).Value = clienteempresa.Password;
            comand.Parameters.Add("RUTCOMPLETO", OracleDbType.Varchar2).Value = clienteempresa.Rut;
            comand.Parameters.Add("DIRECCIONX", OracleDbType.Varchar2).Value = clienteempresa.Calle;
            comand.Parameters.Add("NUMEROX", OracleDbType.Int32).Value = clienteempresa.Numerocalle;
            comand.Parameters.Add("MAILX", OracleDbType.Varchar2).Value = clienteempresa.Mail;
            comand.Parameters.Add("TELEFONOX", OracleDbType.Int32).Value = clienteempresa.Telefono;
            comand.Parameters.Add("IDCOMUNAX", OracleDbType.Int32).Value = clienteempresa.Comuna;
            comand.Parameters.Add("NOMBREX", OracleDbType.Varchar2).Value = clienteempresa.Nombreempresa;
            comand.Parameters.Add("ROLX", OracleDbType.Varchar2).Value = clienteempresa.Rol;

            try
            {
                comand.ExecuteNonQuery();
                MessageBox.Show("Cliente Empresa Ingresado");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            comand.Parameters.Clear();

        }

        public void Modificar(ClienteEmpresa clienteempresa)
        {

            comand.Connection = conect.Conectar();
            comand.CommandText = "ps_cliente.modificar_clienteempresa";
            comand.CommandType = CommandType.StoredProcedure;
            comand.Parameters.Add("USUARIO", OracleDbType.Varchar2).Value = clienteempresa.User;
            comand.Parameters.Add("CONTRA", OracleDbType.Varchar2).Value = clienteempresa.Password;
            comand.Parameters.Add("RUTCOMPLETO", OracleDbType.Varchar2).Value = clienteempresa.Rut;
            comand.Parameters.Add("DIRECCIONX", OracleDbType.Varchar2).Value = clienteempresa.Calle;
            comand.Parameters.Add("NUMEROX", OracleDbType.Int32).Value = clienteempresa.Numerocalle;
            comand.Parameters.Add("MAILX", OracleDbType.Varchar2).Value = clienteempresa.Mail;
            comand.Parameters.Add("TELEFONOX", OracleDbType.Int32).Value = clienteempresa.Telefono;
            comand.Parameters.Add("IDCOMUNAX", OracleDbType.Int32).Value = clienteempresa.Comuna;
            comand.Parameters.Add("NOMBREX", OracleDbType.Varchar2).Value = clienteempresa.Nombreempresa;
            comand.Parameters.Add("ROLX", OracleDbType.Varchar2).Value = clienteempresa.Rol;
            comand.Parameters.Add("IDX", OracleDbType.Int64).Value = clienteempresa.Idx;




            try
            {
                comand.ExecuteNonQuery();
                MessageBox.Show("Cliente Empresa Editado");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            comand.Parameters.Clear();

        }


    }
}
