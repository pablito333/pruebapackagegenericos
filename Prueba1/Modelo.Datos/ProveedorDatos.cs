﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;
using System.Data;

namespace Prueba1.Modelo.Datos
{
    public class ProveedorDatos
    {

        private long idx;
        private string nombre;
        private long fono;
        private string rubro;
        private string user;
        private string password;


        private ConexionOracle conect = new ConexionOracle();
        private OracleCommand comand = new OracleCommand();
        OracleDataAdapter objAdapter = new OracleDataAdapter();
        DataTable dt = new DataTable();

        public long Idx { get => idx; set => idx = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public long Fono { get => fono; set => fono = value; }
        public string Rubro { get => rubro; set => rubro = value; }
        public string User { get => user; set => user = value; }
        public string Password { get => password; set => password = value; }

        public ProveedorDatos(long idx, string nombre, long fono, string rubro, string user, string password)
        {
            this.Idx = idx;
            this.Nombre = nombre;
            this.Fono = fono;
            this.Rubro = rubro;
            this.User = user;
            this.Password = password;
        }

        public ProveedorDatos()
        {
            this.Idx = 0;
            this.Nombre = "";
            this.Fono = 0;
            this.Rubro = "";
            this.User = "";
            this.Password = "";
        }

        public DataTable Mostrar()
        {

            comand.Connection = conect.Conectar();
            comand.CommandText = "ps_proveedor.cargar_proveedor";
            comand.BindByName = true;
            comand.CommandType = CommandType.StoredProcedure;
            comand.Parameters.Add("proveedor", OracleDbType.RefCursor).Direction = ParameterDirection.ReturnValue;
            objAdapter.SelectCommand = comand;
            objAdapter.Fill(dt);
            comand.Parameters.Clear();
            return dt;
        }


        public void Insertar(ProveedorDatos proveedor) {

            comand.Connection = conect.Conectar();
            comand.CommandText = "ps_proveedor.agregar_proveedor";
            comand.CommandType = CommandType.StoredProcedure;
            comand.Parameters.Add("NOMBREX", OracleDbType.Varchar2, 25).Value = proveedor.Nombre;
            comand.Parameters.Add("FONOX", OracleDbType.Int64, 12).Value = proveedor.Fono;
            comand.Parameters.Add("RUBROX", OracleDbType.Varchar2, 50).Value = proveedor.Rubro;
            comand.Parameters.Add("USERX", OracleDbType.Varchar2, 50).Value = proveedor.User;
            comand.Parameters.Add("PASSX", OracleDbType.Varchar2, 50).Value = proveedor.Password;


            try
            {
                comand.ExecuteNonQuery();
                MessageBox.Show("Proveedor Ingresado");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            comand.Parameters.Clear();
        }
        public void Modificar(ProveedorDatos proveedor)
        {

            comand.Connection = conect.Conectar();
            comand.CommandText = "ps_proveedor.modificar_proveedor";
            comand.CommandType = CommandType.StoredProcedure;
            comand.Parameters.Add("NOMBREX", OracleDbType.Varchar2, 25).Value = proveedor.Nombre;
            comand.Parameters.Add("FONOX", OracleDbType.Int64, 12).Value = proveedor.Fono;
            comand.Parameters.Add("RUBROX", OracleDbType.Varchar2, 50).Value = proveedor.Rubro;
            comand.Parameters.Add("USERX", OracleDbType.Varchar2, 50).Value = proveedor.User;
            comand.Parameters.Add("PASSX", OracleDbType.Varchar2, 50).Value = proveedor.Password;
            comand.Parameters.Add("IDX", OracleDbType.Int64, 12).Value = proveedor.Idx;


            try
            {
                comand.ExecuteNonQuery();
                MessageBox.Show("Proveedor Editado");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            comand.Parameters.Clear();
        }
        public void Eliminar(ProveedorDatos proveedor)
        {
            comand.Connection = conect.Conectar();
            comand.CommandText = "ps_proveedor.eliminar_proveedor";
            comand.BindByName = true;
            comand.CommandType = CommandType.StoredProcedure;
            comand.Parameters.Add("IDX", OracleDbType.Int64, 10).Value = proveedor.Idx; ;
            try
            {
                comand.ExecuteNonQuery();
                MessageBox.Show("Proveedor Eliminado");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            comand.Parameters.Clear();

        }

    }
}
