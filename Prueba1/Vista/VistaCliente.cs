﻿using Prueba1.Controlador.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Oracle.DataAccess.Client;

namespace Prueba1.Vista
{
    public partial class VistaCliente : Form
    {

        ClienteNegocio client = new ClienteNegocio();
        private bool Modificar = false;
        private String idx = "";
        public VistaCliente()
        {
            InitializeComponent();
            CargarComboBoxComunas();
            dataGridCliente.ReadOnly = true;
            CheckBoxNaturalEmpresa.Checked = true;
        }

        private void BotonCerrarCliente_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BotonMinimizarCliente_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void BotonRestaurarCliente_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            BotonRestaurarCliente.Visible = false;
            BotonMaximizarCliente.Visible = true;
        }

        private void BotonMaximizarCliente_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            BotonMaximizarCliente.Visible = false;
            BotonRestaurarCliente.Visible = true;
        }

        private void MostrarTabla()
        {
            ClienteNegocio cliente = new ClienteNegocio();
            dataGridCliente.DataSource = cliente.MostrarCliente();
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        private void PanelSupProducto_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void VistaCliente_Load(object sender, EventArgs e)
        {
            MostrarTabla();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBoxNaturalEmpresa.Checked == true)
            {
                this.PanelNatural.Visible = true;
                this.PaneEmpresa.Visible = false;
            }
            if (CheckBoxNaturalEmpresa.Checked == false)
            {
                this.PanelNatural.Visible = false;
                this.PaneEmpresa.Visible = true;
            }
        }

        private void BotonGuardar_Persona_Click(object sender, EventArgs e)
        {

            if (Modificar == false) { 
            try
            {
                String fkcomuna = comboboxcomuna.SelectedValue.ToString();
                
                ClienteNegocio clientenatural = new ClienteNegocio();
                clientenatural.InsertarClientePersona(txtuser.Text, txtpassword.Text, txtrut_persona.Text, txtcalle.Text, txtnumerocalle.Text, txtmail.Text, txttelefono.Text, fkcomuna, txtnombre_persona.Text, txtapellido_persona.Text);
                MostrarTabla();
                Limpiar();



            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
            }
            }
            if (Modificar == true) {

                try { 

                String fkcomuna = comboboxcomuna.SelectedValue.ToString();

                ClienteNegocio clientenatural = new ClienteNegocio();
                clientenatural.ModificarClientePersona(txtuser.Text, txtpassword.Text, txtrut_persona.Text, txtcalle.Text, txtnumerocalle.Text, txtmail.Text, txttelefono.Text, fkcomuna, txtnombre_persona.Text, txtapellido_persona.Text,idx);
                MostrarTabla();
                BotonGuardar_Persona.Text = "Guardar";
                Limpiar();
                Modificar = false;
                CheckBoxNaturalEmpresa.Enabled = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex);
                }


            }





    }
        private void Limpiar() {

            txtuser.Clear();
            txtpassword.Clear();
            txtcalle.Clear();
            txtnumerocalle.Clear();
            txtmail.Clear();
            txttelefono.Clear();
            txtrut_persona.Clear();
            txtnombre_persona.Clear();
            txtapellido_persona.Clear();
            txtnombre_empresa.Clear();
            txtrol_empresa.Clear();
            comboboxcomuna.SelectedIndex = 0;
            dateTime_persona.Value = DateTime.Now;

        }

        //CARGAR COMBO BOX DE COMUNAS DESDE SP
        private void CargarComboBoxComunas() {

            ConexionOracle con = new ConexionOracle();
            OracleCommand cmd = new OracleCommand();
            OracleDataAdapter objAdapter = new OracleDataAdapter();
            DataTable dt = new DataTable();

            cmd.Connection = con.GetOracleConnection();
            cmd.CommandText = "ps_genericos.cargar_comuna";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("cursorcomuna", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            objAdapter.SelectCommand = cmd;

            objAdapter.Fill(dt);
            comboboxcomuna.DataSource = dt;
            comboboxcomuna.DisplayMember = "DESCRIPCION_COMUNA";
            comboboxcomuna.ValueMember = "ID_COMUNA";
        }

        private void BotonEliminar_Click(object sender, EventArgs e)
        {
            if (dataGridCliente.SelectedRows.Count > 0)
            {
                idx = dataGridCliente.CurrentRow.Cells["ID_CLIENTE"].Value.ToString();
                client.EliminarCliente(idx);
                MostrarTabla();
                Limpiar();
            }
            else
            {

                MessageBox.Show("Seleccione Fila");
            }
        }

        private void BotonModificar_Click(object sender, EventArgs e)
        {
            if (dataGridCliente.SelectedRows.Count > 0)
            {
                idx = dataGridCliente.CurrentRow.Cells["ID_CLIENTE"].Value.ToString();
                txtuser.Text = dataGridCliente.CurrentRow.Cells["USER_CLIENTE"].Value.ToString();
                txtpassword.Text = dataGridCliente.CurrentRow.Cells["PASS_CLIENTE"].Value.ToString();
                txtcalle.Text = dataGridCliente.CurrentRow.Cells["DIRECCION_CALLE"].Value.ToString();
                txtnumerocalle.Text = dataGridCliente.CurrentRow.Cells["DIRECCION_NUMERO"].Value.ToString();
                txtmail.Text = dataGridCliente.CurrentRow.Cells["EMAIL"].Value.ToString();
                txttelefono.Text = dataGridCliente.CurrentRow.Cells["TELEFONO"].Value.ToString();
                comboboxcomuna.Text = dataGridCliente.CurrentRow.Cells["COMUNA"].Value.ToString();
                String rutnatural = dataGridCliente.CurrentRow.Cells["RUT_NATURAL"].Value.ToString();
                String rutempresa = dataGridCliente.CurrentRow.Cells["RUT_EMPRESA"].Value.ToString();
                
                if (rutnatural == "") {

                    txtrut_empresa.Text = dataGridCliente.CurrentRow.Cells["RUT_EMPRESA"].Value.ToString();
                    txtnombre_empresa.Text = dataGridCliente.CurrentRow.Cells["nombre_empresa"].Value.ToString();
                    txtrol_empresa.Text = dataGridCliente.CurrentRow.Cells["rol_empresa"].Value.ToString();
                    CheckBoxNaturalEmpresa.Checked = false;
                    BotonGuardar_Empresa.Text = "Modificar";
                    Modificar = true;
                    CheckBoxNaturalEmpresa.Enabled = false;
                }
                else if (rutempresa == "") { 
                

                
                    txtrut_persona.Text = dataGridCliente.CurrentRow.Cells["RUT_NATURAL"].Value.ToString();
                    txtnombre_persona.Text = dataGridCliente.CurrentRow.Cells["nombre_natural"].Value.ToString();
                    txtapellido_persona.Text = dataGridCliente.CurrentRow.Cells["apellido_natural"].Value.ToString();
                    CheckBoxNaturalEmpresa.Checked = true;
                    BotonGuardar_Persona.Text = "Modificar";
                    Modificar = true;
                    CheckBoxNaturalEmpresa.Enabled = false;
                }
            }
            else
            {

                MessageBox.Show("Seleccione Fila");
            }
        }


        //BOTON GUARDA O MODIFICA EMPRESA 
        private void BotonGuardar_Empresa_Click(object sender, EventArgs e)
        {
            //SI MODIFICAR==FALSE INSERTA, SI ES TRUE MODIFICA 

            if (Modificar == false)
            {
                try
                {
                    String fkcomuna = comboboxcomuna.SelectedValue.ToString();

                    ClienteNegocio clienteempresa = new ClienteNegocio();
                    clienteempresa.InsertarClienteEmpresa(txtuser.Text, txtpassword.Text, txtrut_empresa.Text, txtcalle.Text, txtnumerocalle.Text, txtmail.Text, txttelefono.Text, fkcomuna, txtnombre_empresa.Text,txtrol_empresa.Text);
                    MostrarTabla();
                    Limpiar();



                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex);
                }
            }
            if (Modificar == true)
            {

                try
                {

                    String fkcomuna = comboboxcomuna.SelectedValue.ToString();

                    ClienteNegocio clienteempresa = new ClienteNegocio();
                    clienteempresa.ModificarClienteEmpresa(txtuser.Text, txtpassword.Text, txtrut_empresa.Text, txtcalle.Text, txtnumerocalle.Text, txtmail.Text, txttelefono.Text, fkcomuna, txtnombre_empresa.Text, txtrol_empresa.Text, idx);
                    MostrarTabla();
                    BotonGuardar_Empresa.Text = "Guardar";
                    Limpiar();
                    Modificar = false;
                    CheckBoxNaturalEmpresa.Enabled = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex);
                }


            }











        }
    }
}
