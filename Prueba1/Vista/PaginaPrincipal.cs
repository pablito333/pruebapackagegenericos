﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Prueba1.Vista
{
    public partial class PaginaPrincipal : Form
    {
        private String user;
        public PaginaPrincipal(String user)
        {
            InitializeComponent();
            this.user = user;
            laber_user.Text = user;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BotonMaximize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            BotonMaximize.Visible = false;
            BotonRestaurar.Visible = true;

        }

        private void BotonRestaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            BotonRestaurar.Visible = false;
            BotonMaximize.Visible = true;

        }

        private void BotonMiniminaze_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

       

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BotonProducto_Click(object sender, EventArgs e)
        {
            VistaProducto vistaProducto = new VistaProducto();
            vistaProducto.Visible = true;
            PaneSubClienteProveedores.Visible = false;

        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void PanelTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void PaginaPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void BotonClientesProveedores_Click(object sender, EventArgs e)
        {
            PaneSubClienteProveedores.Visible = true;
        }

        private void BotonClientes_Click(object sender, EventArgs e)
        {
            VistaCliente vistacliente = new VistaCliente();
            vistacliente.Visible = true;
        }

        private void BotonProveedores_Click(object sender, EventArgs e)
        {
            VistaProvedores vistaprovedores = new VistaProvedores();
            vistaprovedores.Visible = true; 
        }

        private void BotonFuncionarios_Click(object sender, EventArgs e)
        {
            VistaFuncionario vistafuncionario = new VistaFuncionario();
            vistafuncionario.Visible = true;
        }
    }
}
