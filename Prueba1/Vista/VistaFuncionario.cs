﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Oracle.DataAccess.Client;
using Prueba1.Controlador.Negocio;

namespace Prueba1.Vista
{
    public partial class VistaFuncionario : Form
    {
        private bool Modificar = false;
        private String idx = "";
        private string user;
        private string rut;
        public VistaFuncionario()
        {
            InitializeComponent();
            dataGridFuncionario.ReadOnly = true;
            CargoComboBoxCargos();
        }

        //FUNCION QUE CARGA EL COMBOBOXCARGOS

        private void CargoComboBoxCargos() {
            ConexionOracle con = new ConexionOracle();
            OracleCommand cmd = new OracleCommand();
            OracleDataAdapter objAdapter = new OracleDataAdapter();
            DataTable dt = new DataTable();

            cmd.Connection = con.GetOracleConnection();
            cmd.CommandText = "ps_genericos.cargar_cargo";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("cursorcargo", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            objAdapter.SelectCommand = cmd;

            objAdapter.Fill(dt);
            comboBoxFuncionario.DataSource = dt;
            comboBoxFuncionario.DisplayMember = "DESCRIPCION_CARGO";
            comboBoxFuncionario.ValueMember = "ID_CARGO";
        }

        private void BotonCerrarFuncionario_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BotonMaximizarFuncionario_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            BotonMaximizarFuncionario.Visible = false;
            BotonRestaurarFuncionario.Visible = true;
        }

        private void BotonMinimizarFuncionario_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void BotonRestaurarFuncionario_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            BotonRestaurarFuncionario.Visible = false;
            BotonMaximizarFuncionario.Visible = true;
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        private void PanelSupFuncionario_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void VistaFuncionario_Load(object sender, EventArgs e)
        {
            MostrarTabla();
        }

        private void MostrarTabla()
        {
            FuncionarioNegocio funcionario = new FuncionarioNegocio();
            dataGridFuncionario.DataSource = funcionario.MostrarFuncionario();
        }
        public void Limpiar() {

            textrut.Clear();
            textnombre.Clear();
            textapellido.Clear();
            textuser.Clear();
            textpass.Clear();
            comboBoxFuncionario.SelectedIndex = 0;

        }
        public bool ValidateEmpty(Form formulario) {
            bool validate = false;
            if(textrut.Text == "" || textnombre.Text == "" || textapellido.Text == "" || textuser.Text == "" || textpass.Text == "")
            {
                MessageBox.Show("Debe rellenar todos los campos");
                validate = true;
            }
            else 
            {

                if (textrut.Text.Length < 8)
                {
                    MessageBox.Show("Rut debe tener minimo 8 caracteres");
                    validate = true;
                }
                else 
                {
                    if (rut != textrut.Text)
                    {
                        FuncionarioNegocio funcionario = new FuncionarioNegocio();
                        int validaterut = funcionario.ValidarRutFuncionario(textrut.Text);
                        if (validaterut == 1)
                        {
                            MessageBox.Show("Rut ya existe");
                            validate = true;
                        }
                    }
                }
                if (textnombre.Text.Length < 4)
                {
                    MessageBox.Show("Nombre debe tener minimo 4 caracteres");
                    validate = true;
                }
                if (textapellido.Text.Length < 4)
                {
                    MessageBox.Show("Apellido debe tener minimo 4 caracteres");
                    validate = true;
                }
                if (textuser.Text.Length < 6)
                {
                    MessageBox.Show("Usser debe tener minimo 6 caracteres");
                    validate = true;
                }
                else
                {
                    FuncionarioNegocio funcionario = new FuncionarioNegocio();
                    if (user != textuser.Text) {
                        int validaterut = funcionario.ValidarUserFuncionario(textuser.Text);
                        if (validaterut == 1)
                        {
                            MessageBox.Show("Usuario ya existe");
                            validate = true;
                        }
                    }       
                }
                if (textpass.Text.Length < 6)
                {
                    MessageBox.Show("Pasword debe tener minimo 6 caracteres");
                    validate = true;
                }
            }
            return validate;

        }
        
        private void BotonGuardar_Click(object sender, EventArgs e)
        {
            if (Modificar == false)
            {
                bool validateempty = ValidateEmpty(this);
                if (validateempty == false)
                {

                    try
                    {
                        String cargo = comboBoxFuncionario.SelectedValue.ToString();
                        FuncionarioNegocio funcionario = new FuncionarioNegocio();
                        funcionario.InsertarFuncionario(textrut.Text, textnombre.Text, textapellido.Text, textuser.Text, textpass.Text, cargo);
                        MostrarTabla();
                        Limpiar();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex);
                    }




                }


            }
            if (Modificar == true)
            {

                bool validateempty = ValidateEmpty(this);
                if (validateempty == false)
                {
                    try
                    {
                        String cargo = comboBoxFuncionario.SelectedValue.ToString();
                        FuncionarioNegocio funcionario = new FuncionarioNegocio();
                        funcionario.ModificarFuncionario(textrut.Text, textnombre.Text, textapellido.Text, textuser.Text, textpass.Text, cargo, idx);
                        textrut.ReadOnly = false;
                        MostrarTabla();
                        Limpiar();
                        BotonGuardar.Text = "Guardar";
                        Modificar = false;
                        Limpiar();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex);
                    }
                }

            }
        }

        private void BotonModificar_Click(object sender, EventArgs e)
        {
            if (dataGridFuncionario.SelectedRows.Count > 0)
            {

                rut = textrut.Text = dataGridFuncionario.CurrentRow.Cells["RUT_FUNCIONARIO"].Value.ToString();
                textnombre.Text = dataGridFuncionario.CurrentRow.Cells["nombre_funcionario"].Value.ToString();
                textapellido.Text = dataGridFuncionario.CurrentRow.Cells["APELLIDO_FUNCIONARIO"].Value.ToString();
                user = textuser.Text = dataGridFuncionario.CurrentRow.Cells["USER_FUNCIONARIO"].Value.ToString();
                textpass.Text = dataGridFuncionario.CurrentRow.Cells["PASS_FUNCIONARIO"].Value.ToString();
                comboBoxFuncionario.Text = dataGridFuncionario.CurrentRow.Cells["CARGO"].Value.ToString();
                idx = dataGridFuncionario.CurrentRow.Cells["RUT_FUNCIONARIO"].Value.ToString();
                Modificar = true;
                BotonGuardar.Text = "Confirmar Modificacion";
                textrut.ReadOnly = true;
            }
            else
            {

                MessageBox.Show("Seleccione Fila");
            }
        }

        private void BotonEliminar_Click(object sender, EventArgs e)
        {
            if (dataGridFuncionario.SelectedRows.Count > 0)
            {
                FuncionarioNegocio funcionario = new FuncionarioNegocio();
                idx = dataGridFuncionario.CurrentRow.Cells["RUT_FUNCIONARIO"].Value.ToString();
                funcionario.Eliminar(idx);
                MostrarTabla();
                Limpiar();

            }
            else
            {

                MessageBox.Show("Seleccione Fila");
            }
        }

        private void textrut_KeyPress(object sender, KeyPressEventArgs e)
        {
           
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (e.KeyChar.ToString().ToUpper().Equals("K"))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                MessageBox.Show("Solo se permiten numeros y letra K", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textnombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten letras", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textapellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten letras", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textuser_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsSymbol(e.KeyChar))
            {
                MessageBox.Show("Solo se permiten letras y numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void comboBoxFuncionario_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
}
