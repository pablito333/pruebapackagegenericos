﻿namespace Prueba1.Vista
{
    partial class VistaFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VistaFuncionario));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PanelSupFuncionario = new System.Windows.Forms.Panel();
            this.BotonRestaurarFuncionario = new System.Windows.Forms.PictureBox();
            this.BotonMaximizarFuncionario = new System.Windows.Forms.PictureBox();
            this.BotonMinimizarFuncionario = new System.Windows.Forms.PictureBox();
            this.BotonCerrarFuncionario = new System.Windows.Forms.PictureBox();
            this.dataGridFuncionario = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBoxFuncionario = new System.Windows.Forms.ComboBox();
            this.BotonGuardar = new System.Windows.Forms.Button();
            this.textpass = new System.Windows.Forms.TextBox();
            this.textuser = new System.Windows.Forms.TextBox();
            this.textapellido = new System.Windows.Forms.TextBox();
            this.textnombre = new System.Windows.Forms.TextBox();
            this.textrut = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BotonEliminar = new System.Windows.Forms.Button();
            this.BotonModificar = new System.Windows.Forms.Button();
            this.PanelSupFuncionario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BotonRestaurarFuncionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMaximizarFuncionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMinimizarFuncionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonCerrarFuncionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridFuncionario)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelSupFuncionario
            // 
            this.PanelSupFuncionario.BackColor = System.Drawing.Color.MediumBlue;
            this.PanelSupFuncionario.Controls.Add(this.BotonRestaurarFuncionario);
            this.PanelSupFuncionario.Controls.Add(this.BotonMaximizarFuncionario);
            this.PanelSupFuncionario.Controls.Add(this.BotonMinimizarFuncionario);
            this.PanelSupFuncionario.Controls.Add(this.BotonCerrarFuncionario);
            this.PanelSupFuncionario.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelSupFuncionario.Location = new System.Drawing.Point(0, 0);
            this.PanelSupFuncionario.Name = "PanelSupFuncionario";
            this.PanelSupFuncionario.Size = new System.Drawing.Size(1084, 45);
            this.PanelSupFuncionario.TabIndex = 1;
            this.PanelSupFuncionario.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PanelSupFuncionario_MouseDown);
            // 
            // BotonRestaurarFuncionario
            // 
            this.BotonRestaurarFuncionario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonRestaurarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonRestaurarFuncionario.Image = ((System.Drawing.Image)(resources.GetObject("BotonRestaurarFuncionario.Image")));
            this.BotonRestaurarFuncionario.Location = new System.Drawing.Point(1028, 0);
            this.BotonRestaurarFuncionario.Name = "BotonRestaurarFuncionario";
            this.BotonRestaurarFuncionario.Size = new System.Drawing.Size(25, 25);
            this.BotonRestaurarFuncionario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonRestaurarFuncionario.TabIndex = 1;
            this.BotonRestaurarFuncionario.TabStop = false;
            this.BotonRestaurarFuncionario.Visible = false;
            this.BotonRestaurarFuncionario.Click += new System.EventHandler(this.BotonRestaurarFuncionario_Click);
            // 
            // BotonMaximizarFuncionario
            // 
            this.BotonMaximizarFuncionario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonMaximizarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonMaximizarFuncionario.Image = ((System.Drawing.Image)(resources.GetObject("BotonMaximizarFuncionario.Image")));
            this.BotonMaximizarFuncionario.Location = new System.Drawing.Point(1028, 0);
            this.BotonMaximizarFuncionario.Name = "BotonMaximizarFuncionario";
            this.BotonMaximizarFuncionario.Size = new System.Drawing.Size(25, 25);
            this.BotonMaximizarFuncionario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonMaximizarFuncionario.TabIndex = 1;
            this.BotonMaximizarFuncionario.TabStop = false;
            this.BotonMaximizarFuncionario.Click += new System.EventHandler(this.BotonMaximizarFuncionario_Click);
            // 
            // BotonMinimizarFuncionario
            // 
            this.BotonMinimizarFuncionario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonMinimizarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonMinimizarFuncionario.ErrorImage = ((System.Drawing.Image)(resources.GetObject("BotonMinimizarFuncionario.ErrorImage")));
            this.BotonMinimizarFuncionario.Image = ((System.Drawing.Image)(resources.GetObject("BotonMinimizarFuncionario.Image")));
            this.BotonMinimizarFuncionario.Location = new System.Drawing.Point(997, 0);
            this.BotonMinimizarFuncionario.Name = "BotonMinimizarFuncionario";
            this.BotonMinimizarFuncionario.Size = new System.Drawing.Size(25, 25);
            this.BotonMinimizarFuncionario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonMinimizarFuncionario.TabIndex = 1;
            this.BotonMinimizarFuncionario.TabStop = false;
            this.BotonMinimizarFuncionario.Click += new System.EventHandler(this.BotonMinimizarFuncionario_Click);
            // 
            // BotonCerrarFuncionario
            // 
            this.BotonCerrarFuncionario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonCerrarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonCerrarFuncionario.Image = ((System.Drawing.Image)(resources.GetObject("BotonCerrarFuncionario.Image")));
            this.BotonCerrarFuncionario.Location = new System.Drawing.Point(1059, 0);
            this.BotonCerrarFuncionario.Name = "BotonCerrarFuncionario";
            this.BotonCerrarFuncionario.Size = new System.Drawing.Size(25, 25);
            this.BotonCerrarFuncionario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonCerrarFuncionario.TabIndex = 1;
            this.BotonCerrarFuncionario.TabStop = false;
            this.BotonCerrarFuncionario.Click += new System.EventHandler(this.BotonCerrarFuncionario_Click);
            // 
            // dataGridFuncionario
            // 
            this.dataGridFuncionario.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridFuncionario.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridFuncionario.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridFuncionario.BackgroundColor = System.Drawing.Color.DarkBlue;
            this.dataGridFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridFuncionario.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.dataGridFuncionario.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridFuncionario.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridFuncionario.ColumnHeadersHeight = 30;
            this.dataGridFuncionario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridFuncionario.EnableHeadersVisualStyles = false;
            this.dataGridFuncionario.GridColor = System.Drawing.Color.RoyalBlue;
            this.dataGridFuncionario.Location = new System.Drawing.Point(45, 51);
            this.dataGridFuncionario.Name = "dataGridFuncionario";
            this.dataGridFuncionario.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.DarkBlue;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridFuncionario.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridFuncionario.RowHeadersVisible = false;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.DarkBlue;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.CornflowerBlue;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridFuncionario.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridFuncionario.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridFuncionario.Size = new System.Drawing.Size(614, 278);
            this.dataGridFuncionario.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.MidnightBlue;
            this.panel1.Controls.Add(this.comboBoxFuncionario);
            this.panel1.Controls.Add(this.BotonGuardar);
            this.panel1.Controls.Add(this.textpass);
            this.panel1.Controls.Add(this.textuser);
            this.panel1.Controls.Add(this.textapellido);
            this.panel1.Controls.Add(this.textnombre);
            this.panel1.Controls.Add(this.textrut);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(679, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(405, 370);
            this.panel1.TabIndex = 3;
            // 
            // comboBoxFuncionario
            // 
            this.comboBoxFuncionario.FormattingEnabled = true;
            this.comboBoxFuncionario.Location = new System.Drawing.Point(127, 154);
            this.comboBoxFuncionario.Name = "comboBoxFuncionario";
            this.comboBoxFuncionario.Size = new System.Drawing.Size(216, 21);
            this.comboBoxFuncionario.TabIndex = 17;
            this.comboBoxFuncionario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBoxFuncionario_KeyPress);
            // 
            // BotonGuardar
            // 
            this.BotonGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BotonGuardar.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonGuardar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonGuardar.ForeColor = System.Drawing.Color.White;
            this.BotonGuardar.Location = new System.Drawing.Point(36, 308);
            this.BotonGuardar.Name = "BotonGuardar";
            this.BotonGuardar.Size = new System.Drawing.Size(119, 37);
            this.BotonGuardar.TabIndex = 16;
            this.BotonGuardar.Text = "Guardar";
            this.BotonGuardar.UseVisualStyleBackColor = false;
            this.BotonGuardar.Click += new System.EventHandler(this.BotonGuardar_Click);
            // 
            // textpass
            // 
            this.textpass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textpass.Location = new System.Drawing.Point(127, 264);
            this.textpass.MaxLength = 12;
            this.textpass.Name = "textpass";
            this.textpass.PasswordChar = '*';
            this.textpass.Size = new System.Drawing.Size(216, 20);
            this.textpass.TabIndex = 14;
            // 
            // textuser
            // 
            this.textuser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textuser.Location = new System.Drawing.Point(127, 235);
            this.textuser.MaxLength = 12;
            this.textuser.Name = "textuser";
            this.textuser.Size = new System.Drawing.Size(216, 20);
            this.textuser.TabIndex = 13;
            this.textuser.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textuser_KeyPress);
            // 
            // textapellido
            // 
            this.textapellido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textapellido.Location = new System.Drawing.Point(127, 120);
            this.textapellido.MaxLength = 12;
            this.textapellido.Name = "textapellido";
            this.textapellido.Size = new System.Drawing.Size(216, 20);
            this.textapellido.TabIndex = 11;
            this.textapellido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textapellido_KeyPress);
            // 
            // textnombre
            // 
            this.textnombre.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textnombre.Location = new System.Drawing.Point(127, 90);
            this.textnombre.MaxLength = 12;
            this.textnombre.Name = "textnombre";
            this.textnombre.Size = new System.Drawing.Size(216, 20);
            this.textnombre.TabIndex = 10;
            this.textnombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textnombre_KeyPress);
            // 
            // textrut
            // 
            this.textrut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textrut.Location = new System.Drawing.Point(127, 58);
            this.textrut.MaxLength = 9;
            this.textrut.Name = "textrut";
            this.textrut.Size = new System.Drawing.Size(216, 20);
            this.textrut.TabIndex = 9;
            this.textrut.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textrut_KeyPress);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(33, 154);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 20);
            this.label7.TabIndex = 7;
            this.label7.Text = "Cargo :";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(32, 264);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "Password :";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(32, 235);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "User :";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(32, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Apellido :";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(33, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Nombre :";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(33, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Rut :";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SpringGreen;
            this.label1.Location = new System.Drawing.Point(32, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(191, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mantenedor Funcionario";
            // 
            // BotonEliminar
            // 
            this.BotonEliminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonEliminar.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonEliminar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonEliminar.ForeColor = System.Drawing.Color.White;
            this.BotonEliminar.Location = new System.Drawing.Point(540, 353);
            this.BotonEliminar.Name = "BotonEliminar";
            this.BotonEliminar.Size = new System.Drawing.Size(119, 37);
            this.BotonEliminar.TabIndex = 17;
            this.BotonEliminar.Text = "Eliminar";
            this.BotonEliminar.UseVisualStyleBackColor = false;
            this.BotonEliminar.Click += new System.EventHandler(this.BotonEliminar_Click);
            // 
            // BotonModificar
            // 
            this.BotonModificar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonModificar.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonModificar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonModificar.ForeColor = System.Drawing.Color.White;
            this.BotonModificar.Location = new System.Drawing.Point(398, 353);
            this.BotonModificar.Name = "BotonModificar";
            this.BotonModificar.Size = new System.Drawing.Size(119, 37);
            this.BotonModificar.TabIndex = 18;
            this.BotonModificar.Text = "Modificar";
            this.BotonModificar.UseVisualStyleBackColor = false;
            this.BotonModificar.Click += new System.EventHandler(this.BotonModificar_Click);
            // 
            // VistaFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkBlue;
            this.ClientSize = new System.Drawing.Size(1084, 412);
            this.Controls.Add(this.BotonModificar);
            this.Controls.Add(this.BotonEliminar);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridFuncionario);
            this.Controls.Add(this.PanelSupFuncionario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VistaFuncionario";
            this.Text = "VistaFuncionario";
            this.Load += new System.EventHandler(this.VistaFuncionario_Load);
            this.PanelSupFuncionario.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BotonRestaurarFuncionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMaximizarFuncionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMinimizarFuncionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonCerrarFuncionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridFuncionario)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelSupFuncionario;
        private System.Windows.Forms.PictureBox BotonRestaurarFuncionario;
        private System.Windows.Forms.PictureBox BotonMaximizarFuncionario;
        private System.Windows.Forms.PictureBox BotonMinimizarFuncionario;
        private System.Windows.Forms.PictureBox BotonCerrarFuncionario;
        private System.Windows.Forms.DataGridView dataGridFuncionario;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textapellido;
        private System.Windows.Forms.TextBox textnombre;
        private System.Windows.Forms.TextBox textrut;
        private System.Windows.Forms.TextBox textpass;
        private System.Windows.Forms.TextBox textuser;
        private System.Windows.Forms.Button BotonGuardar;
        private System.Windows.Forms.ComboBox comboBoxFuncionario;
        private System.Windows.Forms.Button BotonEliminar;
        private System.Windows.Forms.Button BotonModificar;
    }
}