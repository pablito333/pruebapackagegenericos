﻿namespace Prueba1.Vista
{
    partial class VistaProvedores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VistaProvedores));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PanelSupProveedores = new System.Windows.Forms.Panel();
            this.BotonMaximizarProveedor = new System.Windows.Forms.PictureBox();
            this.BotonRestaurarProveedor = new System.Windows.Forms.PictureBox();
            this.BotonMinimizarProveedor = new System.Windows.Forms.PictureBox();
            this.BotonCerrarProveedor = new System.Windows.Forms.PictureBox();
            this.PanelFormularioProveddor = new System.Windows.Forms.Panel();
            this.BotonGuardar_Proveedor = new System.Windows.Forms.Button();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.txtuser = new System.Windows.Forms.TextBox();
            this.txtrubro = new System.Windows.Forms.TextBox();
            this.txtfono = new System.Windows.Forms.TextBox();
            this.txtnombre = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridProveedor = new System.Windows.Forms.DataGridView();
            this.BotonModificar = new System.Windows.Forms.Button();
            this.BotonEliminar = new System.Windows.Forms.Button();
            this.PanelSupProveedores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMaximizarProveedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonRestaurarProveedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMinimizarProveedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonCerrarProveedor)).BeginInit();
            this.PanelFormularioProveddor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProveedor)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelSupProveedores
            // 
            this.PanelSupProveedores.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelSupProveedores.BackColor = System.Drawing.Color.MediumBlue;
            this.PanelSupProveedores.Controls.Add(this.BotonMaximizarProveedor);
            this.PanelSupProveedores.Controls.Add(this.BotonRestaurarProveedor);
            this.PanelSupProveedores.Controls.Add(this.BotonMinimizarProveedor);
            this.PanelSupProveedores.Controls.Add(this.BotonCerrarProveedor);
            this.PanelSupProveedores.Location = new System.Drawing.Point(2, 0);
            this.PanelSupProveedores.Name = "PanelSupProveedores";
            this.PanelSupProveedores.Size = new System.Drawing.Size(800, 35);
            this.PanelSupProveedores.TabIndex = 1;
            this.PanelSupProveedores.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PanelSupProveedores_MouseDown);
            // 
            // BotonMaximizarProveedor
            // 
            this.BotonMaximizarProveedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonMaximizarProveedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonMaximizarProveedor.Image = ((System.Drawing.Image)(resources.GetObject("BotonMaximizarProveedor.Image")));
            this.BotonMaximizarProveedor.Location = new System.Drawing.Point(742, 7);
            this.BotonMaximizarProveedor.Name = "BotonMaximizarProveedor";
            this.BotonMaximizarProveedor.Size = new System.Drawing.Size(25, 25);
            this.BotonMaximizarProveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonMaximizarProveedor.TabIndex = 2;
            this.BotonMaximizarProveedor.TabStop = false;
            this.BotonMaximizarProveedor.Click += new System.EventHandler(this.BotonMaximizarCliente_Click);
            // 
            // BotonRestaurarProveedor
            // 
            this.BotonRestaurarProveedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonRestaurarProveedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonRestaurarProveedor.Image = ((System.Drawing.Image)(resources.GetObject("BotonRestaurarProveedor.Image")));
            this.BotonRestaurarProveedor.Location = new System.Drawing.Point(742, 7);
            this.BotonRestaurarProveedor.Name = "BotonRestaurarProveedor";
            this.BotonRestaurarProveedor.Size = new System.Drawing.Size(25, 25);
            this.BotonRestaurarProveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonRestaurarProveedor.TabIndex = 2;
            this.BotonRestaurarProveedor.TabStop = false;
            this.BotonRestaurarProveedor.Visible = false;
            this.BotonRestaurarProveedor.Click += new System.EventHandler(this.BotonRestaurarCliente_Click);
            // 
            // BotonMinimizarProveedor
            // 
            this.BotonMinimizarProveedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonMinimizarProveedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonMinimizarProveedor.ErrorImage = ((System.Drawing.Image)(resources.GetObject("BotonMinimizarProveedor.ErrorImage")));
            this.BotonMinimizarProveedor.Image = ((System.Drawing.Image)(resources.GetObject("BotonMinimizarProveedor.Image")));
            this.BotonMinimizarProveedor.Location = new System.Drawing.Point(711, 7);
            this.BotonMinimizarProveedor.Name = "BotonMinimizarProveedor";
            this.BotonMinimizarProveedor.Size = new System.Drawing.Size(25, 25);
            this.BotonMinimizarProveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonMinimizarProveedor.TabIndex = 3;
            this.BotonMinimizarProveedor.TabStop = false;
            this.BotonMinimizarProveedor.Click += new System.EventHandler(this.BotonMinimizarCliente_Click);
            // 
            // BotonCerrarProveedor
            // 
            this.BotonCerrarProveedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonCerrarProveedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonCerrarProveedor.Image = ((System.Drawing.Image)(resources.GetObject("BotonCerrarProveedor.Image")));
            this.BotonCerrarProveedor.Location = new System.Drawing.Point(772, 7);
            this.BotonCerrarProveedor.Name = "BotonCerrarProveedor";
            this.BotonCerrarProveedor.Size = new System.Drawing.Size(25, 25);
            this.BotonCerrarProveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonCerrarProveedor.TabIndex = 2;
            this.BotonCerrarProveedor.TabStop = false;
            this.BotonCerrarProveedor.Click += new System.EventHandler(this.BotonCerrarCliente_Click);
            // 
            // PanelFormularioProveddor
            // 
            this.PanelFormularioProveddor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.PanelFormularioProveddor.BackColor = System.Drawing.Color.MidnightBlue;
            this.PanelFormularioProveddor.Controls.Add(this.BotonGuardar_Proveedor);
            this.PanelFormularioProveddor.Controls.Add(this.txtpassword);
            this.PanelFormularioProveddor.Controls.Add(this.txtuser);
            this.PanelFormularioProveddor.Controls.Add(this.txtrubro);
            this.PanelFormularioProveddor.Controls.Add(this.txtfono);
            this.PanelFormularioProveddor.Controls.Add(this.txtnombre);
            this.PanelFormularioProveddor.Controls.Add(this.label6);
            this.PanelFormularioProveddor.Controls.Add(this.label5);
            this.PanelFormularioProveddor.Controls.Add(this.label4);
            this.PanelFormularioProveddor.Controls.Add(this.label3);
            this.PanelFormularioProveddor.Controls.Add(this.label2);
            this.PanelFormularioProveddor.Controls.Add(this.label1);
            this.PanelFormularioProveddor.Location = new System.Drawing.Point(2, 34);
            this.PanelFormularioProveddor.Name = "PanelFormularioProveddor";
            this.PanelFormularioProveddor.Size = new System.Drawing.Size(332, 721);
            this.PanelFormularioProveddor.TabIndex = 3;
            // 
            // BotonGuardar_Proveedor
            // 
            this.BotonGuardar_Proveedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BotonGuardar_Proveedor.Location = new System.Drawing.Point(155, 559);
            this.BotonGuardar_Proveedor.Name = "BotonGuardar_Proveedor";
            this.BotonGuardar_Proveedor.Size = new System.Drawing.Size(133, 39);
            this.BotonGuardar_Proveedor.TabIndex = 35;
            this.BotonGuardar_Proveedor.Text = "Guardar";
            this.BotonGuardar_Proveedor.UseVisualStyleBackColor = true;
            this.BotonGuardar_Proveedor.Click += new System.EventHandler(this.BotonGuardar_Proveedor_Click);
            // 
            // txtpassword
            // 
            this.txtpassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtpassword.Location = new System.Drawing.Point(102, 247);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.Size = new System.Drawing.Size(186, 20);
            this.txtpassword.TabIndex = 34;
            // 
            // txtuser
            // 
            this.txtuser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtuser.Location = new System.Drawing.Point(102, 214);
            this.txtuser.Name = "txtuser";
            this.txtuser.Size = new System.Drawing.Size(186, 20);
            this.txtuser.TabIndex = 33;
            // 
            // txtrubro
            // 
            this.txtrubro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtrubro.Location = new System.Drawing.Point(102, 151);
            this.txtrubro.Name = "txtrubro";
            this.txtrubro.Size = new System.Drawing.Size(186, 20);
            this.txtrubro.TabIndex = 32;
            // 
            // txtfono
            // 
            this.txtfono.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtfono.Location = new System.Drawing.Point(102, 112);
            this.txtfono.Name = "txtfono";
            this.txtfono.Size = new System.Drawing.Size(186, 20);
            this.txtfono.TabIndex = 31;
            // 
            // txtnombre
            // 
            this.txtnombre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtnombre.Location = new System.Drawing.Point(102, 77);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(186, 20);
            this.txtnombre.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(10, 247);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 20);
            this.label6.TabIndex = 29;
            this.label6.Text = "Password :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(10, 214);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 20);
            this.label5.TabIndex = 28;
            this.label5.Text = "User :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(10, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 20);
            this.label4.TabIndex = 27;
            this.label4.Text = "Rubro :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(10, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 20);
            this.label3.TabIndex = 26;
            this.label3.Text = "Fono :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(10, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 20);
            this.label2.TabIndex = 25;
            this.label2.Text = "Nombre :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SpringGreen;
            this.label1.Location = new System.Drawing.Point(10, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Mantenedor Proveedor";
            // 
            // dataGridProveedor
            // 
            this.dataGridProveedor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridProveedor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridProveedor.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridProveedor.BackgroundColor = System.Drawing.Color.DarkBlue;
            this.dataGridProveedor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridProveedor.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.dataGridProveedor.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridProveedor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridProveedor.ColumnHeadersHeight = 30;
            this.dataGridProveedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridProveedor.EnableHeadersVisualStyles = false;
            this.dataGridProveedor.GridColor = System.Drawing.Color.RoyalBlue;
            this.dataGridProveedor.Location = new System.Drawing.Point(390, 61);
            this.dataGridProveedor.Name = "dataGridProveedor";
            this.dataGridProveedor.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DarkBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridProveedor.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridProveedor.RowHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DarkBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.CornflowerBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridProveedor.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridProveedor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridProveedor.Size = new System.Drawing.Size(385, 511);
            this.dataGridProveedor.TabIndex = 20;
            // 
            // BotonModificar
            // 
            this.BotonModificar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BotonModificar.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonModificar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonModificar.ForeColor = System.Drawing.Color.White;
            this.BotonModificar.Location = new System.Drawing.Point(372, 593);
            this.BotonModificar.Name = "BotonModificar";
            this.BotonModificar.Size = new System.Drawing.Size(133, 39);
            this.BotonModificar.TabIndex = 21;
            this.BotonModificar.Text = "Modificar";
            this.BotonModificar.UseVisualStyleBackColor = false;
            this.BotonModificar.Click += new System.EventHandler(this.BotonModificar_Click);
            // 
            // BotonEliminar
            // 
            this.BotonEliminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BotonEliminar.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonEliminar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonEliminar.ForeColor = System.Drawing.Color.White;
            this.BotonEliminar.Location = new System.Drawing.Point(523, 593);
            this.BotonEliminar.Name = "BotonEliminar";
            this.BotonEliminar.Size = new System.Drawing.Size(133, 39);
            this.BotonEliminar.TabIndex = 22;
            this.BotonEliminar.Text = "Eliminar";
            this.BotonEliminar.UseVisualStyleBackColor = false;
            this.BotonEliminar.Click += new System.EventHandler(this.BotonEliminar_Click);
            // 
            // VistaProvedores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkBlue;
            this.ClientSize = new System.Drawing.Size(800, 750);
            this.Controls.Add(this.BotonEliminar);
            this.Controls.Add(this.BotonModificar);
            this.Controls.Add(this.dataGridProveedor);
            this.Controls.Add(this.PanelFormularioProveddor);
            this.Controls.Add(this.PanelSupProveedores);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VistaProvedores";
            this.Load += new System.EventHandler(this.VistaProvedores_Load);
            this.PanelSupProveedores.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BotonMaximizarProveedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonRestaurarProveedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMinimizarProveedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonCerrarProveedor)).EndInit();
            this.PanelFormularioProveddor.ResumeLayout(false);
            this.PanelFormularioProveddor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProveedor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelSupProveedores;
        private System.Windows.Forms.PictureBox BotonMaximizarProveedor;
        private System.Windows.Forms.PictureBox BotonRestaurarProveedor;
        private System.Windows.Forms.PictureBox BotonMinimizarProveedor;
        private System.Windows.Forms.PictureBox BotonCerrarProveedor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel PanelFormularioProveddor;
        private System.Windows.Forms.DataGridView dataGridProveedor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtpassword;
        private System.Windows.Forms.TextBox txtuser;
        private System.Windows.Forms.TextBox txtrubro;
        private System.Windows.Forms.TextBox txtfono;
        private System.Windows.Forms.TextBox txtnombre;
        private System.Windows.Forms.Button BotonGuardar_Proveedor;
        private System.Windows.Forms.Button BotonModificar;
        private System.Windows.Forms.Button BotonEliminar;
    }
}