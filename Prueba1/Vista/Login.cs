﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Text;
using Prueba1.Modelo.Datos;
using Prueba1.Controlador.Negocio;

namespace Prueba1.Vista
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wparam, int lparam);

        private void textUser_Enter(object sender, EventArgs e)
        {
            if(textUser.Text == "USUARIO")
            {
                textUser.Text = "";
                textUser.ForeColor = Color.LightGray;
            }
        }

        private void textUser_Leave(object sender, EventArgs e)
        {
            if (textUser.Text == "")
            {
                textUser.Text = "USUARIO";
                textUser.ForeColor = Color.DimGray;
            }
        }

        private void textPass_Enter(object sender, EventArgs e)
        {
            if (textPass.Text == "CONTRASEÑA")
            {
                textPass.Text = "";
                textPass.ForeColor = Color.LightGray;
                textPass.UseSystemPasswordChar = true;
            }
        }

        private void textPass_Leave(object sender, EventArgs e)
        {
            if (textPass.Text == "")
            {
                textPass.Text = "CONTRASEÑA";
                textPass.ForeColor = Color.DimGray;
                textPass.UseSystemPasswordChar = false;
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void Login_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012);
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (textUser.Text != "USUARIO")
            {
                if (textPass.Text != "CONTRASEÑA")
                {
                    FuncionarioNegocio usuario = new FuncionarioNegocio();
                    var validLogin = usuario.LoginUser(textUser.Text, textPass.Text);
                    if(validLogin == true)
                    {
                        PaginaPrincipal principal = new PaginaPrincipal(textUser.Text);
                        principal.Show();
                        principal.FormClosed += Logout;
                        this.Hide();
                    }
                    else
                    {
                        msgError("Usuario o Contraseña incorrectas. \n  Porfavor intente nuevamente.");
                        textPass.Text = "CONTRASEÑA";
                        textUser.Focus();
                    }
                }
                else msgError("Porfavor Ingrese su contraseña.");
            }
            else msgError("Porfavor Ingrese su nombre de usuario.");
        }
            private void msgError(string msg)
            {
                lblErrorMessage.Text = "" + msg;
                lblErrorMessage.Visible = true;
            }

            private void Logout(object sender, FormClosedEventArgs e)
            {
                textPass.Text = "CONTRASEÑA";
                textPass.UseSystemPasswordChar = false;
                textUser.Text = "USUARIO";
                lblErrorMessage.Visible = false;       
                this.Show();
            }

        private void lblErrorMessage_Click(object sender, EventArgs e)
        {

        }

         private void textUser_TextChanged(object sender, EventArgs e)
        {

        }

        private void linkPass_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            RecuperarPass recupera = new RecuperarPass();
            recupera.Show();
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMaximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            btnMinimizar.Visible = false;
            btnMaximizar.Visible = true;
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            btnMaximizar.Visible = false;
            btnMinimizar.Visible = true;
        }
    }  
}
