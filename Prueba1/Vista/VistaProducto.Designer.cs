﻿namespace Prueba1.Vista
{
    partial class VistaProducto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VistaProducto));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PanelSupProducto = new System.Windows.Forms.Panel();
            this.BotonRestaurarProduct = new System.Windows.Forms.PictureBox();
            this.BotonMaximizarProducto = new System.Windows.Forms.PictureBox();
            this.BotonMinimizarProducto = new System.Windows.Forms.PictureBox();
            this.BotonCerrarProducto = new System.Windows.Forms.PictureBox();
            this.dataGridProducto = new System.Windows.Forms.DataGridView();
            this.PanelMantenedor = new System.Windows.Forms.Panel();
            this.BotonGuardar = new System.Windows.Forms.Button();
            this.DateVencimiento = new System.Windows.Forms.DateTimePicker();
            this.comboProducto = new System.Windows.Forms.ComboBox();
            this.comboProveedor = new System.Windows.Forms.ComboBox();
            this.textStockCritico = new System.Windows.Forms.TextBox();
            this.textStock = new System.Windows.Forms.TextBox();
            this.textPrecio = new System.Windows.Forms.TextBox();
            this.textProducto = new System.Windows.Forms.TextBox();
            this.CheckBoxVencimiento = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BotonModificar = new System.Windows.Forms.Button();
            this.BotonEliminar = new System.Windows.Forms.Button();
            this.PanelSupProducto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BotonRestaurarProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMaximizarProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMinimizarProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonCerrarProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProducto)).BeginInit();
            this.PanelMantenedor.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelSupProducto
            // 
            this.PanelSupProducto.BackColor = System.Drawing.Color.MediumBlue;
            this.PanelSupProducto.Controls.Add(this.BotonRestaurarProduct);
            this.PanelSupProducto.Controls.Add(this.BotonMaximizarProducto);
            this.PanelSupProducto.Controls.Add(this.BotonMinimizarProducto);
            this.PanelSupProducto.Controls.Add(this.BotonCerrarProducto);
            this.PanelSupProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelSupProducto.Location = new System.Drawing.Point(0, 0);
            this.PanelSupProducto.Name = "PanelSupProducto";
            this.PanelSupProducto.Size = new System.Drawing.Size(1100, 35);
            this.PanelSupProducto.TabIndex = 0;
            this.PanelSupProducto.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PanelSupProducto_MouseDown);
            // 
            // BotonRestaurarProduct
            // 
            this.BotonRestaurarProduct.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonRestaurarProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonRestaurarProduct.Image = ((System.Drawing.Image)(resources.GetObject("BotonRestaurarProduct.Image")));
            this.BotonRestaurarProduct.Location = new System.Drawing.Point(1044, 0);
            this.BotonRestaurarProduct.Name = "BotonRestaurarProduct";
            this.BotonRestaurarProduct.Size = new System.Drawing.Size(25, 25);
            this.BotonRestaurarProduct.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonRestaurarProduct.TabIndex = 1;
            this.BotonRestaurarProduct.TabStop = false;
            this.BotonRestaurarProduct.Visible = false;
            this.BotonRestaurarProduct.Click += new System.EventHandler(this.BotonRestaurarProduct_Click);
            // 
            // BotonMaximizarProducto
            // 
            this.BotonMaximizarProducto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonMaximizarProducto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonMaximizarProducto.Image = ((System.Drawing.Image)(resources.GetObject("BotonMaximizarProducto.Image")));
            this.BotonMaximizarProducto.Location = new System.Drawing.Point(1044, 0);
            this.BotonMaximizarProducto.Name = "BotonMaximizarProducto";
            this.BotonMaximizarProducto.Size = new System.Drawing.Size(25, 25);
            this.BotonMaximizarProducto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonMaximizarProducto.TabIndex = 1;
            this.BotonMaximizarProducto.TabStop = false;
            this.BotonMaximizarProducto.Click += new System.EventHandler(this.BotonMaximizarProducto_Click);
            // 
            // BotonMinimizarProducto
            // 
            this.BotonMinimizarProducto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonMinimizarProducto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonMinimizarProducto.ErrorImage = ((System.Drawing.Image)(resources.GetObject("BotonMinimizarProducto.ErrorImage")));
            this.BotonMinimizarProducto.Image = ((System.Drawing.Image)(resources.GetObject("BotonMinimizarProducto.Image")));
            this.BotonMinimizarProducto.Location = new System.Drawing.Point(1013, 0);
            this.BotonMinimizarProducto.Name = "BotonMinimizarProducto";
            this.BotonMinimizarProducto.Size = new System.Drawing.Size(25, 25);
            this.BotonMinimizarProducto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonMinimizarProducto.TabIndex = 1;
            this.BotonMinimizarProducto.TabStop = false;
            this.BotonMinimizarProducto.Click += new System.EventHandler(this.BotonMinimizarProducto_Click);
            // 
            // BotonCerrarProducto
            // 
            this.BotonCerrarProducto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonCerrarProducto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonCerrarProducto.Image = ((System.Drawing.Image)(resources.GetObject("BotonCerrarProducto.Image")));
            this.BotonCerrarProducto.Location = new System.Drawing.Point(1075, 0);
            this.BotonCerrarProducto.Name = "BotonCerrarProducto";
            this.BotonCerrarProducto.Size = new System.Drawing.Size(25, 25);
            this.BotonCerrarProducto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonCerrarProducto.TabIndex = 1;
            this.BotonCerrarProducto.TabStop = false;
            this.BotonCerrarProducto.Click += new System.EventHandler(this.BotonCerrarProducto_Click);
            // 
            // dataGridProducto
            // 
            this.dataGridProducto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridProducto.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridProducto.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridProducto.BackgroundColor = System.Drawing.Color.DarkBlue;
            this.dataGridProducto.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridProducto.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.dataGridProducto.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridProducto.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridProducto.ColumnHeadersHeight = 30;
            this.dataGridProducto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridProducto.EnableHeadersVisualStyles = false;
            this.dataGridProducto.GridColor = System.Drawing.Color.RoyalBlue;
            this.dataGridProducto.Location = new System.Drawing.Point(12, 61);
            this.dataGridProducto.Name = "dataGridProducto";
            this.dataGridProducto.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DarkBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridProducto.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridProducto.RowHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DarkBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.CornflowerBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridProducto.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridProducto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridProducto.Size = new System.Drawing.Size(614, 325);
            this.dataGridProducto.TabIndex = 1;
            // 
            // PanelMantenedor
            // 
            this.PanelMantenedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelMantenedor.BackColor = System.Drawing.Color.MidnightBlue;
            this.PanelMantenedor.Controls.Add(this.BotonGuardar);
            this.PanelMantenedor.Controls.Add(this.DateVencimiento);
            this.PanelMantenedor.Controls.Add(this.comboProducto);
            this.PanelMantenedor.Controls.Add(this.comboProveedor);
            this.PanelMantenedor.Controls.Add(this.textStockCritico);
            this.PanelMantenedor.Controls.Add(this.textStock);
            this.PanelMantenedor.Controls.Add(this.textPrecio);
            this.PanelMantenedor.Controls.Add(this.textProducto);
            this.PanelMantenedor.Controls.Add(this.CheckBoxVencimiento);
            this.PanelMantenedor.Controls.Add(this.label7);
            this.PanelMantenedor.Controls.Add(this.label6);
            this.PanelMantenedor.Controls.Add(this.label5);
            this.PanelMantenedor.Controls.Add(this.label4);
            this.PanelMantenedor.Controls.Add(this.label3);
            this.PanelMantenedor.Controls.Add(this.label2);
            this.PanelMantenedor.Controls.Add(this.label1);
            this.PanelMantenedor.Location = new System.Drawing.Point(664, 35);
            this.PanelMantenedor.Name = "PanelMantenedor";
            this.PanelMantenedor.Size = new System.Drawing.Size(436, 415);
            this.PanelMantenedor.TabIndex = 2;
            // 
            // BotonGuardar
            // 
            this.BotonGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonGuardar.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonGuardar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonGuardar.ForeColor = System.Drawing.Color.White;
            this.BotonGuardar.Location = new System.Drawing.Point(94, 327);
            this.BotonGuardar.Name = "BotonGuardar";
            this.BotonGuardar.Size = new System.Drawing.Size(244, 39);
            this.BotonGuardar.TabIndex = 15;
            this.BotonGuardar.Text = "Guardar";
            this.BotonGuardar.UseVisualStyleBackColor = false;
            this.BotonGuardar.Click += new System.EventHandler(this.BotonGuardar_Click);
            // 
            // DateVencimiento
            // 
            this.DateVencimiento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DateVencimiento.CalendarFont = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateVencimiento.CalendarForeColor = System.Drawing.Color.White;
            this.DateVencimiento.CalendarTitleBackColor = System.Drawing.Color.Navy;
            this.DateVencimiento.Location = new System.Drawing.Point(181, 285);
            this.DateVencimiento.Name = "DateVencimiento";
            this.DateVencimiento.Size = new System.Drawing.Size(200, 20);
            this.DateVencimiento.TabIndex = 14;
            this.DateVencimiento.Value = new System.DateTime(2020, 5, 15, 0, 0, 0, 0);
            this.DateVencimiento.Visible = false;
            // 
            // comboProducto
            // 
            this.comboProducto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboProducto.FormattingEnabled = true;
            this.comboProducto.Location = new System.Drawing.Point(165, 250);
            this.comboProducto.Name = "comboProducto";
            this.comboProducto.Size = new System.Drawing.Size(216, 21);
            this.comboProducto.TabIndex = 13;
            // 
            // comboProveedor
            // 
            this.comboProveedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboProveedor.FormattingEnabled = true;
            this.comboProveedor.Location = new System.Drawing.Point(165, 214);
            this.comboProveedor.Name = "comboProveedor";
            this.comboProveedor.Size = new System.Drawing.Size(216, 21);
            this.comboProveedor.TabIndex = 12;
            // 
            // textStockCritico
            // 
            this.textStockCritico.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textStockCritico.Location = new System.Drawing.Point(165, 171);
            this.textStockCritico.Name = "textStockCritico";
            this.textStockCritico.Size = new System.Drawing.Size(216, 20);
            this.textStockCritico.TabIndex = 11;
            // 
            // textStock
            // 
            this.textStock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textStock.Location = new System.Drawing.Point(165, 137);
            this.textStock.Name = "textStock";
            this.textStock.Size = new System.Drawing.Size(216, 20);
            this.textStock.TabIndex = 10;
            // 
            // textPrecio
            // 
            this.textPrecio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textPrecio.Location = new System.Drawing.Point(165, 100);
            this.textPrecio.Name = "textPrecio";
            this.textPrecio.Size = new System.Drawing.Size(216, 20);
            this.textPrecio.TabIndex = 9;
            // 
            // textProducto
            // 
            this.textProducto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textProducto.Location = new System.Drawing.Point(165, 65);
            this.textProducto.Name = "textProducto";
            this.textProducto.Size = new System.Drawing.Size(216, 20);
            this.textProducto.TabIndex = 8;
            // 
            // CheckBoxVencimiento
            // 
            this.CheckBoxVencimiento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckBoxVencimiento.AutoSize = true;
            this.CheckBoxVencimiento.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckBoxVencimiento.ForeColor = System.Drawing.Color.White;
            this.CheckBoxVencimiento.Location = new System.Drawing.Point(54, 285);
            this.CheckBoxVencimiento.Name = "CheckBoxVencimiento";
            this.CheckBoxVencimiento.Size = new System.Drawing.Size(131, 20);
            this.CheckBoxVencimiento.TabIndex = 7;
            this.CheckBoxVencimiento.Text = "Fecha Vencimiento";
            this.CheckBoxVencimiento.UseVisualStyleBackColor = true;
            this.CheckBoxVencimiento.CheckedChanged += new System.EventHandler(this.CheckBoxVencimiento_CheckedChanged);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(50, 250);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Tipo Producto :";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(50, 210);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Proveedor :";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(50, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Stock Critico :";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(50, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Stock :";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(50, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Precio (CLP) :";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(50, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Producto :";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SpringGreen;
            this.label1.Location = new System.Drawing.Point(24, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mantenedor Producto";
            // 
            // BotonModificar
            // 
            this.BotonModificar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BotonModificar.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonModificar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonModificar.ForeColor = System.Drawing.Color.White;
            this.BotonModificar.Location = new System.Drawing.Point(12, 391);
            this.BotonModificar.Name = "BotonModificar";
            this.BotonModificar.Size = new System.Drawing.Size(244, 39);
            this.BotonModificar.TabIndex = 16;
            this.BotonModificar.Text = "Modificar";
            this.BotonModificar.UseVisualStyleBackColor = false;
            this.BotonModificar.Click += new System.EventHandler(this.BotonModificar_Click);
            // 
            // BotonEliminar
            // 
            this.BotonEliminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonEliminar.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonEliminar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonEliminar.ForeColor = System.Drawing.Color.White;
            this.BotonEliminar.Location = new System.Drawing.Point(375, 392);
            this.BotonEliminar.Name = "BotonEliminar";
            this.BotonEliminar.Size = new System.Drawing.Size(251, 39);
            this.BotonEliminar.TabIndex = 17;
            this.BotonEliminar.Text = "Eliminar";
            this.BotonEliminar.UseVisualStyleBackColor = false;
            this.BotonEliminar.Click += new System.EventHandler(this.BotonEliminar_Click);
            // 
            // VistaProducto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkBlue;
            this.ClientSize = new System.Drawing.Size(1100, 450);
            this.Controls.Add(this.BotonEliminar);
            this.Controls.Add(this.BotonModificar);
            this.Controls.Add(this.PanelMantenedor);
            this.Controls.Add(this.dataGridProducto);
            this.Controls.Add(this.PanelSupProducto);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VistaProducto";
            this.Text = "VistaProducto";
            this.Load += new System.EventHandler(this.VistaProducto_Load);
            this.PanelSupProducto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BotonRestaurarProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMaximizarProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMinimizarProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonCerrarProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProducto)).EndInit();
            this.PanelMantenedor.ResumeLayout(false);
            this.PanelMantenedor.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelSupProducto;
        private System.Windows.Forms.PictureBox BotonCerrarProducto;
        private System.Windows.Forms.PictureBox BotonMinimizarProducto;
        private System.Windows.Forms.PictureBox BotonMaximizarProducto;
        private System.Windows.Forms.PictureBox BotonRestaurarProduct;
        private System.Windows.Forms.DataGridView dataGridProducto;
        private System.Windows.Forms.Panel PanelMantenedor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboProducto;
        private System.Windows.Forms.ComboBox comboProveedor;
        private System.Windows.Forms.TextBox textStockCritico;
        private System.Windows.Forms.TextBox textStock;
        private System.Windows.Forms.TextBox textPrecio;
        private System.Windows.Forms.TextBox textProducto;
        private System.Windows.Forms.CheckBox CheckBoxVencimiento;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker DateVencimiento;
        private System.Windows.Forms.Button BotonGuardar;
        private System.Windows.Forms.Button BotonModificar;
        private System.Windows.Forms.Button BotonEliminar;
    }
}