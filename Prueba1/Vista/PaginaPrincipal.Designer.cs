﻿namespace Prueba1.Vista
{
    partial class PaginaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PaginaPrincipal));
            this.PanelTitulo = new System.Windows.Forms.Panel();
            this.laber_user = new System.Windows.Forms.Label();
            this.BotonRestaurar = new System.Windows.Forms.PictureBox();
            this.BotonMiniminaze = new System.Windows.Forms.PictureBox();
            this.BotonMaximize = new System.Windows.Forms.PictureBox();
            this.BotonExit = new System.Windows.Forms.PictureBox();
            this.PanelVertical = new System.Windows.Forms.Panel();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.BotonReportes = new System.Windows.Forms.Button();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.BotonFuncionarios = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.BotonOrdendecompra = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.BotonClientesProveedores = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.BotonVenta = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.BotonProducto = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PaneSubClienteProveedores = new System.Windows.Forms.Panel();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.BotonProveedores = new System.Windows.Forms.Button();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.BotonClientes = new System.Windows.Forms.Button();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.PanelTitulo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BotonRestaurar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMiniminaze)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMaximize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonExit)).BeginInit();
            this.PanelVertical.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.PaneSubClienteProveedores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelTitulo
            // 
            this.PanelTitulo.BackColor = System.Drawing.Color.MediumBlue;
            this.PanelTitulo.Controls.Add(this.laber_user);
            this.PanelTitulo.Controls.Add(this.BotonRestaurar);
            this.PanelTitulo.Controls.Add(this.BotonMiniminaze);
            this.PanelTitulo.Controls.Add(this.BotonMaximize);
            this.PanelTitulo.Controls.Add(this.BotonExit);
            this.PanelTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelTitulo.Location = new System.Drawing.Point(0, 0);
            this.PanelTitulo.Name = "PanelTitulo";
            this.PanelTitulo.Size = new System.Drawing.Size(1284, 35);
            this.PanelTitulo.TabIndex = 0;
            this.PanelTitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PanelTitulo_MouseDown);
            // 
            // laber_user
            // 
            this.laber_user.AutoSize = true;
            this.laber_user.BackColor = System.Drawing.Color.DodgerBlue;
            this.laber_user.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.laber_user.Location = new System.Drawing.Point(22, 9);
            this.laber_user.Name = "laber_user";
            this.laber_user.Size = new System.Drawing.Size(54, 18);
            this.laber_user.TabIndex = 1;
            this.laber_user.Text = "label1";
            // 
            // BotonRestaurar
            // 
            this.BotonRestaurar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonRestaurar.BackColor = System.Drawing.Color.MediumBlue;
            this.BotonRestaurar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonRestaurar.Image = ((System.Drawing.Image)(resources.GetObject("BotonRestaurar.Image")));
            this.BotonRestaurar.Location = new System.Drawing.Point(1225, 4);
            this.BotonRestaurar.Name = "BotonRestaurar";
            this.BotonRestaurar.Size = new System.Drawing.Size(25, 25);
            this.BotonRestaurar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonRestaurar.TabIndex = 0;
            this.BotonRestaurar.TabStop = false;
            this.BotonRestaurar.Visible = false;
            this.BotonRestaurar.Click += new System.EventHandler(this.BotonRestaurar_Click);
            // 
            // BotonMiniminaze
            // 
            this.BotonMiniminaze.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonMiniminaze.BackColor = System.Drawing.Color.MediumBlue;
            this.BotonMiniminaze.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonMiniminaze.Image = ((System.Drawing.Image)(resources.GetObject("BotonMiniminaze.Image")));
            this.BotonMiniminaze.Location = new System.Drawing.Point(1194, 4);
            this.BotonMiniminaze.Name = "BotonMiniminaze";
            this.BotonMiniminaze.Size = new System.Drawing.Size(25, 25);
            this.BotonMiniminaze.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonMiniminaze.TabIndex = 0;
            this.BotonMiniminaze.TabStop = false;
            this.BotonMiniminaze.Click += new System.EventHandler(this.BotonMiniminaze_Click);
            // 
            // BotonMaximize
            // 
            this.BotonMaximize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonMaximize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonMaximize.Image = ((System.Drawing.Image)(resources.GetObject("BotonMaximize.Image")));
            this.BotonMaximize.Location = new System.Drawing.Point(1225, 4);
            this.BotonMaximize.Name = "BotonMaximize";
            this.BotonMaximize.Size = new System.Drawing.Size(25, 25);
            this.BotonMaximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonMaximize.TabIndex = 0;
            this.BotonMaximize.TabStop = false;
            this.BotonMaximize.Click += new System.EventHandler(this.BotonMaximize_Click);
            // 
            // BotonExit
            // 
            this.BotonExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonExit.BackColor = System.Drawing.Color.MediumBlue;
            this.BotonExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonExit.ErrorImage = ((System.Drawing.Image)(resources.GetObject("BotonExit.ErrorImage")));
            this.BotonExit.Image = ((System.Drawing.Image)(resources.GetObject("BotonExit.Image")));
            this.BotonExit.Location = new System.Drawing.Point(1256, 3);
            this.BotonExit.Name = "BotonExit";
            this.BotonExit.Size = new System.Drawing.Size(25, 25);
            this.BotonExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonExit.TabIndex = 0;
            this.BotonExit.TabStop = false;
            this.BotonExit.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // PanelVertical
            // 
            this.PanelVertical.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.PanelVertical.Controls.Add(this.pictureBox8);
            this.PanelVertical.Controls.Add(this.pictureBox7);
            this.PanelVertical.Controls.Add(this.BotonReportes);
            this.PanelVertical.Controls.Add(this.pictureBox6);
            this.PanelVertical.Controls.Add(this.BotonFuncionarios);
            this.PanelVertical.Controls.Add(this.pictureBox5);
            this.PanelVertical.Controls.Add(this.BotonOrdendecompra);
            this.PanelVertical.Controls.Add(this.pictureBox4);
            this.PanelVertical.Controls.Add(this.BotonClientesProveedores);
            this.PanelVertical.Controls.Add(this.pictureBox3);
            this.PanelVertical.Controls.Add(this.BotonVenta);
            this.PanelVertical.Controls.Add(this.pictureBox2);
            this.PanelVertical.Controls.Add(this.BotonProducto);
            this.PanelVertical.Controls.Add(this.pictureBox1);
            this.PanelVertical.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelVertical.Location = new System.Drawing.Point(0, 35);
            this.PanelVertical.Name = "PanelVertical";
            this.PanelVertical.Size = new System.Drawing.Size(215, 577);
            this.PanelVertical.TabIndex = 1;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(0, 535);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(38, 42);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 13;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Click += new System.EventHandler(this.pictureBox8_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.MediumBlue;
            this.pictureBox7.Location = new System.Drawing.Point(0, 373);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(10, 30);
            this.pictureBox7.TabIndex = 12;
            this.pictureBox7.TabStop = false;
            // 
            // BotonReportes
            // 
            this.BotonReportes.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonReportes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonReportes.FlatAppearance.BorderSize = 0;
            this.BotonReportes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.BotonReportes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonReportes.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonReportes.ForeColor = System.Drawing.Color.White;
            this.BotonReportes.Image = ((System.Drawing.Image)(resources.GetObject("BotonReportes.Image")));
            this.BotonReportes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BotonReportes.Location = new System.Drawing.Point(0, 373);
            this.BotonReportes.Name = "BotonReportes";
            this.BotonReportes.Size = new System.Drawing.Size(218, 30);
            this.BotonReportes.TabIndex = 11;
            this.BotonReportes.Text = "Reporte";
            this.BotonReportes.UseVisualStyleBackColor = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.MediumBlue;
            this.pictureBox6.Location = new System.Drawing.Point(0, 337);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(10, 30);
            this.pictureBox6.TabIndex = 10;
            this.pictureBox6.TabStop = false;
            // 
            // BotonFuncionarios
            // 
            this.BotonFuncionarios.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonFuncionarios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonFuncionarios.FlatAppearance.BorderSize = 0;
            this.BotonFuncionarios.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.BotonFuncionarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonFuncionarios.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonFuncionarios.ForeColor = System.Drawing.Color.White;
            this.BotonFuncionarios.Image = ((System.Drawing.Image)(resources.GetObject("BotonFuncionarios.Image")));
            this.BotonFuncionarios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BotonFuncionarios.Location = new System.Drawing.Point(0, 337);
            this.BotonFuncionarios.Name = "BotonFuncionarios";
            this.BotonFuncionarios.Size = new System.Drawing.Size(218, 30);
            this.BotonFuncionarios.TabIndex = 9;
            this.BotonFuncionarios.Text = "Funcionarios";
            this.BotonFuncionarios.UseVisualStyleBackColor = false;
            this.BotonFuncionarios.Click += new System.EventHandler(this.BotonFuncionarios_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.MediumBlue;
            this.pictureBox5.Location = new System.Drawing.Point(0, 301);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(10, 30);
            this.pictureBox5.TabIndex = 8;
            this.pictureBox5.TabStop = false;
            // 
            // BotonOrdendecompra
            // 
            this.BotonOrdendecompra.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonOrdendecompra.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonOrdendecompra.FlatAppearance.BorderSize = 0;
            this.BotonOrdendecompra.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.BotonOrdendecompra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonOrdendecompra.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonOrdendecompra.ForeColor = System.Drawing.Color.White;
            this.BotonOrdendecompra.Image = ((System.Drawing.Image)(resources.GetObject("BotonOrdendecompra.Image")));
            this.BotonOrdendecompra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BotonOrdendecompra.Location = new System.Drawing.Point(0, 301);
            this.BotonOrdendecompra.Name = "BotonOrdendecompra";
            this.BotonOrdendecompra.Size = new System.Drawing.Size(218, 30);
            this.BotonOrdendecompra.TabIndex = 7;
            this.BotonOrdendecompra.Text = "Orden de Compra";
            this.BotonOrdendecompra.UseVisualStyleBackColor = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.MediumBlue;
            this.pictureBox4.Location = new System.Drawing.Point(0, 265);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(10, 30);
            this.pictureBox4.TabIndex = 6;
            this.pictureBox4.TabStop = false;
            // 
            // BotonClientesProveedores
            // 
            this.BotonClientesProveedores.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonClientesProveedores.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonClientesProveedores.FlatAppearance.BorderSize = 0;
            this.BotonClientesProveedores.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.BotonClientesProveedores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonClientesProveedores.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonClientesProveedores.ForeColor = System.Drawing.Color.White;
            this.BotonClientesProveedores.Image = ((System.Drawing.Image)(resources.GetObject("BotonClientesProveedores.Image")));
            this.BotonClientesProveedores.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BotonClientesProveedores.Location = new System.Drawing.Point(0, 265);
            this.BotonClientesProveedores.Name = "BotonClientesProveedores";
            this.BotonClientesProveedores.Size = new System.Drawing.Size(218, 30);
            this.BotonClientesProveedores.TabIndex = 5;
            this.BotonClientesProveedores.Text = "Clientes / Proveedores";
            this.BotonClientesProveedores.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BotonClientesProveedores.UseVisualStyleBackColor = false;
            this.BotonClientesProveedores.Click += new System.EventHandler(this.BotonClientesProveedores_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.MediumBlue;
            this.pictureBox3.Location = new System.Drawing.Point(0, 229);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 30);
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            // 
            // BotonVenta
            // 
            this.BotonVenta.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonVenta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonVenta.FlatAppearance.BorderSize = 0;
            this.BotonVenta.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.BotonVenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonVenta.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonVenta.ForeColor = System.Drawing.Color.White;
            this.BotonVenta.Image = ((System.Drawing.Image)(resources.GetObject("BotonVenta.Image")));
            this.BotonVenta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BotonVenta.Location = new System.Drawing.Point(0, 229);
            this.BotonVenta.Name = "BotonVenta";
            this.BotonVenta.Size = new System.Drawing.Size(218, 30);
            this.BotonVenta.TabIndex = 3;
            this.BotonVenta.Text = "Ventas";
            this.BotonVenta.UseVisualStyleBackColor = false;
            this.BotonVenta.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.MediumBlue;
            this.pictureBox2.Location = new System.Drawing.Point(-4, 193);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(14, 30);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // BotonProducto
            // 
            this.BotonProducto.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonProducto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonProducto.FlatAppearance.BorderSize = 0;
            this.BotonProducto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.BotonProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonProducto.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonProducto.ForeColor = System.Drawing.Color.White;
            this.BotonProducto.Image = ((System.Drawing.Image)(resources.GetObject("BotonProducto.Image")));
            this.BotonProducto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BotonProducto.Location = new System.Drawing.Point(0, 193);
            this.BotonProducto.Name = "BotonProducto";
            this.BotonProducto.Size = new System.Drawing.Size(215, 30);
            this.BotonProducto.TabIndex = 1;
            this.BotonProducto.Text = "Producto";
            this.BotonProducto.UseVisualStyleBackColor = false;
            this.BotonProducto.Click += new System.EventHandler(this.BotonProducto_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(215, 187);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // PaneSubClienteProveedores
            // 
            this.PaneSubClienteProveedores.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.PaneSubClienteProveedores.Controls.Add(this.pictureBox10);
            this.PaneSubClienteProveedores.Controls.Add(this.BotonProveedores);
            this.PaneSubClienteProveedores.Controls.Add(this.pictureBox9);
            this.PaneSubClienteProveedores.Controls.Add(this.BotonClientes);
            this.PaneSubClienteProveedores.Location = new System.Drawing.Point(212, 300);
            this.PaneSubClienteProveedores.Name = "PaneSubClienteProveedores";
            this.PaneSubClienteProveedores.Size = new System.Drawing.Size(219, 60);
            this.PaneSubClienteProveedores.TabIndex = 2;
            this.PaneSubClienteProveedores.Visible = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.MediumBlue;
            this.pictureBox10.Location = new System.Drawing.Point(3, 30);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(10, 30);
            this.pictureBox10.TabIndex = 10;
            this.pictureBox10.TabStop = false;
            // 
            // BotonProveedores
            // 
            this.BotonProveedores.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonProveedores.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonProveedores.FlatAppearance.BorderSize = 0;
            this.BotonProveedores.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.BotonProveedores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonProveedores.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonProveedores.ForeColor = System.Drawing.Color.White;
            this.BotonProveedores.Image = ((System.Drawing.Image)(resources.GetObject("BotonProveedores.Image")));
            this.BotonProveedores.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BotonProveedores.Location = new System.Drawing.Point(3, 30);
            this.BotonProveedores.Name = "BotonProveedores";
            this.BotonProveedores.Size = new System.Drawing.Size(218, 30);
            this.BotonProveedores.TabIndex = 9;
            this.BotonProveedores.Text = "Proveedores";
            this.BotonProveedores.UseVisualStyleBackColor = false;
            this.BotonProveedores.Click += new System.EventHandler(this.BotonProveedores_Click);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.MediumBlue;
            this.pictureBox9.Location = new System.Drawing.Point(3, 0);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(10, 30);
            this.pictureBox9.TabIndex = 8;
            this.pictureBox9.TabStop = false;
            // 
            // BotonClientes
            // 
            this.BotonClientes.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonClientes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonClientes.FlatAppearance.BorderSize = 0;
            this.BotonClientes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.BotonClientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonClientes.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonClientes.ForeColor = System.Drawing.Color.White;
            this.BotonClientes.Image = ((System.Drawing.Image)(resources.GetObject("BotonClientes.Image")));
            this.BotonClientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BotonClientes.Location = new System.Drawing.Point(3, 0);
            this.BotonClientes.Name = "BotonClientes";
            this.BotonClientes.Size = new System.Drawing.Size(218, 30);
            this.BotonClientes.TabIndex = 7;
            this.BotonClientes.Text = "Clientes ";
            this.BotonClientes.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BotonClientes.UseVisualStyleBackColor = false;
            this.BotonClientes.Click += new System.EventHandler(this.BotonClientes_Click);
            // 
            // PaginaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(1284, 612);
            this.Controls.Add(this.PaneSubClienteProveedores);
            this.Controls.Add(this.PanelVertical);
            this.Controls.Add(this.PanelTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PaginaPrincipal";
            this.Text = "PaginaPrincipal";
            this.Load += new System.EventHandler(this.PaginaPrincipal_Load);
            this.PanelTitulo.ResumeLayout(false);
            this.PanelTitulo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BotonRestaurar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMiniminaze)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMaximize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonExit)).EndInit();
            this.PanelVertical.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.PaneSubClienteProveedores.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelTitulo;
        private System.Windows.Forms.Panel PanelVertical;
        private System.Windows.Forms.PictureBox BotonExit;
        private System.Windows.Forms.PictureBox BotonMaximize;
        private System.Windows.Forms.PictureBox BotonRestaurar;
        private System.Windows.Forms.PictureBox BotonMiniminaze;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button BotonVenta;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button BotonProducto;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Button BotonFuncionarios;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button BotonOrdendecompra;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button BotonClientesProveedores;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Button BotonReportes;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel PaneSubClienteProveedores;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Button BotonProveedores;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Button BotonClientes;
        private System.Windows.Forms.Label laber_user;
        private System.Windows.Forms.BindingSource bindingSource1;
    }
}