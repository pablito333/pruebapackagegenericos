﻿namespace Prueba1.Vista
{
    partial class VistaCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VistaCliente));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PanelSupCliente = new System.Windows.Forms.Panel();
            this.BotonMaximizarCliente = new System.Windows.Forms.PictureBox();
            this.BotonRestaurarCliente = new System.Windows.Forms.PictureBox();
            this.BotonMinimizarCliente = new System.Windows.Forms.PictureBox();
            this.BotonCerrarCliente = new System.Windows.Forms.PictureBox();
            this.PanelAgregarCliente = new System.Windows.Forms.Panel();
            this.CheckBoxNaturalEmpresa = new System.Windows.Forms.CheckBox();
            this.PaneEmpresa = new System.Windows.Forms.Panel();
            this.txtrut_empresa = new System.Windows.Forms.TextBox();
            this.BotonGuardar_Empresa = new System.Windows.Forms.Button();
            this.txtrol_empresa = new System.Windows.Forms.TextBox();
            this.txtnombre_empresa = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.PanelNatural = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.BotonGuardar_Persona = new System.Windows.Forms.Button();
            this.dateTime_persona = new System.Windows.Forms.DateTimePicker();
            this.txtapellido_persona = new System.Windows.Forms.TextBox();
            this.txtnombre_persona = new System.Windows.Forms.TextBox();
            this.txtrut_persona = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboboxcomuna = new System.Windows.Forms.ComboBox();
            this.txttelefono = new System.Windows.Forms.TextBox();
            this.txtmail = new System.Windows.Forms.TextBox();
            this.txtnumerocalle = new System.Windows.Forms.TextBox();
            this.txtcalle = new System.Windows.Forms.TextBox();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.txtuser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BotonModificar = new System.Windows.Forms.Button();
            this.BotonEliminar = new System.Windows.Forms.Button();
            this.dataGridCliente = new System.Windows.Forms.DataGridView();
            this.PanelSupCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMaximizarCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonRestaurarCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMinimizarCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonCerrarCliente)).BeginInit();
            this.PanelAgregarCliente.SuspendLayout();
            this.PaneEmpresa.SuspendLayout();
            this.PanelNatural.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCliente)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelSupCliente
            // 
            this.PanelSupCliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelSupCliente.BackColor = System.Drawing.Color.MediumBlue;
            this.PanelSupCliente.Controls.Add(this.BotonMaximizarCliente);
            this.PanelSupCliente.Controls.Add(this.BotonRestaurarCliente);
            this.PanelSupCliente.Controls.Add(this.BotonMinimizarCliente);
            this.PanelSupCliente.Controls.Add(this.BotonCerrarCliente);
            this.PanelSupCliente.Location = new System.Drawing.Point(0, 1);
            this.PanelSupCliente.Name = "PanelSupCliente";
            this.PanelSupCliente.Size = new System.Drawing.Size(800, 35);
            this.PanelSupCliente.TabIndex = 0;
            this.PanelSupCliente.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PanelSupProducto_MouseDown);
            // 
            // BotonMaximizarCliente
            // 
            this.BotonMaximizarCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonMaximizarCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonMaximizarCliente.Image = ((System.Drawing.Image)(resources.GetObject("BotonMaximizarCliente.Image")));
            this.BotonMaximizarCliente.Location = new System.Drawing.Point(742, 7);
            this.BotonMaximizarCliente.Name = "BotonMaximizarCliente";
            this.BotonMaximizarCliente.Size = new System.Drawing.Size(25, 25);
            this.BotonMaximizarCliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonMaximizarCliente.TabIndex = 2;
            this.BotonMaximizarCliente.TabStop = false;
            this.BotonMaximizarCliente.Click += new System.EventHandler(this.BotonMaximizarCliente_Click);
            // 
            // BotonRestaurarCliente
            // 
            this.BotonRestaurarCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonRestaurarCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonRestaurarCliente.Image = ((System.Drawing.Image)(resources.GetObject("BotonRestaurarCliente.Image")));
            this.BotonRestaurarCliente.Location = new System.Drawing.Point(742, 7);
            this.BotonRestaurarCliente.Name = "BotonRestaurarCliente";
            this.BotonRestaurarCliente.Size = new System.Drawing.Size(25, 25);
            this.BotonRestaurarCliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonRestaurarCliente.TabIndex = 2;
            this.BotonRestaurarCliente.TabStop = false;
            this.BotonRestaurarCliente.Visible = false;
            this.BotonRestaurarCliente.Click += new System.EventHandler(this.BotonRestaurarCliente_Click);
            // 
            // BotonMinimizarCliente
            // 
            this.BotonMinimizarCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonMinimizarCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonMinimizarCliente.ErrorImage = ((System.Drawing.Image)(resources.GetObject("BotonMinimizarCliente.ErrorImage")));
            this.BotonMinimizarCliente.Image = ((System.Drawing.Image)(resources.GetObject("BotonMinimizarCliente.Image")));
            this.BotonMinimizarCliente.Location = new System.Drawing.Point(711, 7);
            this.BotonMinimizarCliente.Name = "BotonMinimizarCliente";
            this.BotonMinimizarCliente.Size = new System.Drawing.Size(25, 25);
            this.BotonMinimizarCliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonMinimizarCliente.TabIndex = 3;
            this.BotonMinimizarCliente.TabStop = false;
            this.BotonMinimizarCliente.Click += new System.EventHandler(this.BotonMinimizarCliente_Click);
            // 
            // BotonCerrarCliente
            // 
            this.BotonCerrarCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonCerrarCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BotonCerrarCliente.Image = ((System.Drawing.Image)(resources.GetObject("BotonCerrarCliente.Image")));
            this.BotonCerrarCliente.Location = new System.Drawing.Point(772, 7);
            this.BotonCerrarCliente.Name = "BotonCerrarCliente";
            this.BotonCerrarCliente.Size = new System.Drawing.Size(25, 25);
            this.BotonCerrarCliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BotonCerrarCliente.TabIndex = 2;
            this.BotonCerrarCliente.TabStop = false;
            this.BotonCerrarCliente.Click += new System.EventHandler(this.BotonCerrarCliente_Click);
            // 
            // PanelAgregarCliente
            // 
            this.PanelAgregarCliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelAgregarCliente.BackColor = System.Drawing.Color.MidnightBlue;
            this.PanelAgregarCliente.Controls.Add(this.CheckBoxNaturalEmpresa);
            this.PanelAgregarCliente.Controls.Add(this.PaneEmpresa);
            this.PanelAgregarCliente.Controls.Add(this.PanelNatural);
            this.PanelAgregarCliente.Controls.Add(this.label8);
            this.PanelAgregarCliente.Controls.Add(this.label7);
            this.PanelAgregarCliente.Controls.Add(this.label6);
            this.PanelAgregarCliente.Controls.Add(this.label5);
            this.PanelAgregarCliente.Controls.Add(this.label4);
            this.PanelAgregarCliente.Controls.Add(this.label3);
            this.PanelAgregarCliente.Controls.Add(this.label2);
            this.PanelAgregarCliente.Controls.Add(this.comboboxcomuna);
            this.PanelAgregarCliente.Controls.Add(this.txttelefono);
            this.PanelAgregarCliente.Controls.Add(this.txtmail);
            this.PanelAgregarCliente.Controls.Add(this.txtnumerocalle);
            this.PanelAgregarCliente.Controls.Add(this.txtcalle);
            this.PanelAgregarCliente.Controls.Add(this.txtpassword);
            this.PanelAgregarCliente.Controls.Add(this.txtuser);
            this.PanelAgregarCliente.Controls.Add(this.label1);
            this.PanelAgregarCliente.Location = new System.Drawing.Point(0, 35);
            this.PanelAgregarCliente.Name = "PanelAgregarCliente";
            this.PanelAgregarCliente.Size = new System.Drawing.Size(800, 277);
            this.PanelAgregarCliente.TabIndex = 1;
            // 
            // CheckBoxNaturalEmpresa
            // 
            this.CheckBoxNaturalEmpresa.AutoSize = true;
            this.CheckBoxNaturalEmpresa.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckBoxNaturalEmpresa.ForeColor = System.Drawing.Color.White;
            this.CheckBoxNaturalEmpresa.Location = new System.Drawing.Point(319, 7);
            this.CheckBoxNaturalEmpresa.Name = "CheckBoxNaturalEmpresa";
            this.CheckBoxNaturalEmpresa.Size = new System.Drawing.Size(221, 24);
            this.CheckBoxNaturalEmpresa.TabIndex = 33;
            this.CheckBoxNaturalEmpresa.Text = "Persona Natural / Empresa";
            this.CheckBoxNaturalEmpresa.UseVisualStyleBackColor = true;
            this.CheckBoxNaturalEmpresa.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // PaneEmpresa
            // 
            this.PaneEmpresa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PaneEmpresa.BackColor = System.Drawing.Color.DarkBlue;
            this.PaneEmpresa.Controls.Add(this.txtrut_empresa);
            this.PaneEmpresa.Controls.Add(this.BotonGuardar_Empresa);
            this.PaneEmpresa.Controls.Add(this.txtrol_empresa);
            this.PaneEmpresa.Controls.Add(this.txtnombre_empresa);
            this.PaneEmpresa.Controls.Add(this.label17);
            this.PaneEmpresa.Controls.Add(this.label16);
            this.PaneEmpresa.Controls.Add(this.label15);
            this.PaneEmpresa.Controls.Add(this.label10);
            this.PaneEmpresa.Location = new System.Drawing.Point(565, 33);
            this.PaneEmpresa.Name = "PaneEmpresa";
            this.PaneEmpresa.Size = new System.Drawing.Size(235, 230);
            this.PaneEmpresa.TabIndex = 32;
            this.PaneEmpresa.Visible = false;
            // 
            // txtrut_empresa
            // 
            this.txtrut_empresa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtrut_empresa.Location = new System.Drawing.Point(74, 41);
            this.txtrut_empresa.Name = "txtrut_empresa";
            this.txtrut_empresa.Size = new System.Drawing.Size(161, 20);
            this.txtrut_empresa.TabIndex = 33;
            // 
            // BotonGuardar_Empresa
            // 
            this.BotonGuardar_Empresa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonGuardar_Empresa.Location = new System.Drawing.Point(71, 186);
            this.BotonGuardar_Empresa.Name = "BotonGuardar_Empresa";
            this.BotonGuardar_Empresa.Size = new System.Drawing.Size(120, 37);
            this.BotonGuardar_Empresa.TabIndex = 32;
            this.BotonGuardar_Empresa.Text = "Guardar";
            this.BotonGuardar_Empresa.UseVisualStyleBackColor = true;
            this.BotonGuardar_Empresa.Click += new System.EventHandler(this.BotonGuardar_Empresa_Click);
            // 
            // txtrol_empresa
            // 
            this.txtrol_empresa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtrol_empresa.Location = new System.Drawing.Point(71, 107);
            this.txtrol_empresa.Name = "txtrol_empresa";
            this.txtrol_empresa.Size = new System.Drawing.Size(161, 20);
            this.txtrol_empresa.TabIndex = 31;
            // 
            // txtnombre_empresa
            // 
            this.txtnombre_empresa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtnombre_empresa.Location = new System.Drawing.Point(71, 79);
            this.txtnombre_empresa.Name = "txtnombre_empresa";
            this.txtnombre_empresa.Size = new System.Drawing.Size(161, 20);
            this.txtnombre_empresa.TabIndex = 30;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(4, 105);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(39, 20);
            this.label17.TabIndex = 29;
            this.label17.Text = "Rol :";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(5, 82);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 17);
            this.label16.TabIndex = 27;
            this.label16.Text = "Nombre :";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(4, 39);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 20);
            this.label15.TabIndex = 26;
            this.label15.Text = "Rut :";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.SpringGreen;
            this.label10.Location = new System.Drawing.Point(4, 1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 20);
            this.label10.TabIndex = 3;
            this.label10.Text = "Empresa ";
            // 
            // PanelNatural
            // 
            this.PanelNatural.BackColor = System.Drawing.Color.DarkBlue;
            this.PanelNatural.Controls.Add(this.label14);
            this.PanelNatural.Controls.Add(this.label13);
            this.PanelNatural.Controls.Add(this.label12);
            this.PanelNatural.Controls.Add(this.label11);
            this.PanelNatural.Controls.Add(this.BotonGuardar_Persona);
            this.PanelNatural.Controls.Add(this.dateTime_persona);
            this.PanelNatural.Controls.Add(this.txtapellido_persona);
            this.PanelNatural.Controls.Add(this.txtnombre_persona);
            this.PanelNatural.Controls.Add(this.txtrut_persona);
            this.PanelNatural.Controls.Add(this.label9);
            this.PanelNatural.Location = new System.Drawing.Point(319, 33);
            this.PanelNatural.Name = "PanelNatural";
            this.PanelNatural.Size = new System.Drawing.Size(240, 230);
            this.PanelNatural.TabIndex = 31;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(58, 140);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 20);
            this.label14.TabIndex = 28;
            this.label14.Text = "Nacimiento :";
            this.label14.Visible = false;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(-1, 105);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 20);
            this.label13.TabIndex = 27;
            this.label13.Text = "Apellido :";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(0, 79);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 20);
            this.label12.TabIndex = 26;
            this.label12.Text = "Nombre :";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(3, 39);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 20);
            this.label11.TabIndex = 25;
            this.label11.Text = "Rut :";
            // 
            // BotonGuardar_Persona
            // 
            this.BotonGuardar_Persona.Location = new System.Drawing.Point(51, 190);
            this.BotonGuardar_Persona.Name = "BotonGuardar_Persona";
            this.BotonGuardar_Persona.Size = new System.Drawing.Size(120, 37);
            this.BotonGuardar_Persona.TabIndex = 22;
            this.BotonGuardar_Persona.Text = "Guardar";
            this.BotonGuardar_Persona.UseVisualStyleBackColor = true;
            this.BotonGuardar_Persona.Click += new System.EventHandler(this.BotonGuardar_Persona_Click);
            // 
            // dateTime_persona
            // 
            this.dateTime_persona.Location = new System.Drawing.Point(35, 163);
            this.dateTime_persona.Name = "dateTime_persona";
            this.dateTime_persona.Size = new System.Drawing.Size(161, 20);
            this.dateTime_persona.TabIndex = 21;
            this.dateTime_persona.TabStop = false;
            this.dateTime_persona.Value = new System.DateTime(2020, 5, 30, 0, 0, 0, 0);
            this.dateTime_persona.Visible = false;
            // 
            // txtapellido_persona
            // 
            this.txtapellido_persona.Location = new System.Drawing.Point(76, 105);
            this.txtapellido_persona.Name = "txtapellido_persona";
            this.txtapellido_persona.Size = new System.Drawing.Size(161, 20);
            this.txtapellido_persona.TabIndex = 20;
            // 
            // txtnombre_persona
            // 
            this.txtnombre_persona.Location = new System.Drawing.Point(76, 79);
            this.txtnombre_persona.Name = "txtnombre_persona";
            this.txtnombre_persona.Size = new System.Drawing.Size(161, 20);
            this.txtnombre_persona.TabIndex = 19;
            // 
            // txtrut_persona
            // 
            this.txtrut_persona.Location = new System.Drawing.Point(76, 39);
            this.txtrut_persona.Name = "txtrut_persona";
            this.txtrut_persona.Size = new System.Drawing.Size(161, 20);
            this.txtrut_persona.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.SpringGreen;
            this.label9.Location = new System.Drawing.Point(3, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(125, 20);
            this.label9.TabIndex = 2;
            this.label9.Text = "Persona Natural";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(12, 236);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 20);
            this.label8.TabIndex = 30;
            this.label8.Text = "Comuna :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(12, 196);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 20);
            this.label7.TabIndex = 29;
            this.label7.Text = "Telefono :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(12, 161);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 20);
            this.label6.TabIndex = 28;
            this.label6.Text = "Mail :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(213, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 20);
            this.label5.TabIndex = 27;
            this.label5.Text = "N° :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(12, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 20);
            this.label4.TabIndex = 26;
            this.label4.Text = "Calle :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(12, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 20);
            this.label3.TabIndex = 25;
            this.label3.Text = "Password :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(12, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 20);
            this.label2.TabIndex = 24;
            this.label2.Text = "User :";
            // 
            // comboboxcomuna
            // 
            this.comboboxcomuna.FormattingEnabled = true;
            this.comboboxcomuna.Location = new System.Drawing.Point(111, 235);
            this.comboboxcomuna.Name = "comboboxcomuna";
            this.comboboxcomuna.Size = new System.Drawing.Size(166, 21);
            this.comboboxcomuna.TabIndex = 23;
            // 
            // txttelefono
            // 
            this.txttelefono.Location = new System.Drawing.Point(111, 196);
            this.txttelefono.Name = "txttelefono";
            this.txttelefono.Size = new System.Drawing.Size(166, 20);
            this.txttelefono.TabIndex = 22;
            // 
            // txtmail
            // 
            this.txtmail.Location = new System.Drawing.Point(111, 161);
            this.txtmail.Name = "txtmail";
            this.txtmail.Size = new System.Drawing.Size(166, 20);
            this.txtmail.TabIndex = 21;
            // 
            // txtnumerocalle
            // 
            this.txtnumerocalle.Location = new System.Drawing.Point(254, 126);
            this.txtnumerocalle.Name = "txtnumerocalle";
            this.txtnumerocalle.Size = new System.Drawing.Size(59, 20);
            this.txtnumerocalle.TabIndex = 20;
            // 
            // txtcalle
            // 
            this.txtcalle.Location = new System.Drawing.Point(74, 126);
            this.txtcalle.Name = "txtcalle";
            this.txtcalle.Size = new System.Drawing.Size(133, 20);
            this.txtcalle.TabIndex = 19;
            // 
            // txtpassword
            // 
            this.txtpassword.Location = new System.Drawing.Point(127, 72);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.Size = new System.Drawing.Size(186, 20);
            this.txtpassword.TabIndex = 18;
            // 
            // txtuser
            // 
            this.txtuser.Location = new System.Drawing.Point(127, 46);
            this.txtuser.Name = "txtuser";
            this.txtuser.Size = new System.Drawing.Size(186, 20);
            this.txtuser.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SpringGreen;
            this.label1.Location = new System.Drawing.Point(12, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mantenedor Cliente";
            // 
            // BotonModificar
            // 
            this.BotonModificar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonModificar.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonModificar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonModificar.ForeColor = System.Drawing.Color.White;
            this.BotonModificar.Location = new System.Drawing.Point(511, 699);
            this.BotonModificar.Name = "BotonModificar";
            this.BotonModificar.Size = new System.Drawing.Size(133, 39);
            this.BotonModificar.TabIndex = 17;
            this.BotonModificar.Text = "Modificar";
            this.BotonModificar.UseVisualStyleBackColor = false;
            this.BotonModificar.Click += new System.EventHandler(this.BotonModificar_Click);
            // 
            // BotonEliminar
            // 
            this.BotonEliminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BotonEliminar.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BotonEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BotonEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonEliminar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonEliminar.ForeColor = System.Drawing.Color.White;
            this.BotonEliminar.Location = new System.Drawing.Point(650, 699);
            this.BotonEliminar.Name = "BotonEliminar";
            this.BotonEliminar.Size = new System.Drawing.Size(133, 39);
            this.BotonEliminar.TabIndex = 18;
            this.BotonEliminar.Text = "Eliminar";
            this.BotonEliminar.UseVisualStyleBackColor = false;
            this.BotonEliminar.Click += new System.EventHandler(this.BotonEliminar_Click);
            // 
            // dataGridCliente
            // 
            this.dataGridCliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridCliente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridCliente.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridCliente.BackgroundColor = System.Drawing.Color.DarkBlue;
            this.dataGridCliente.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridCliente.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.dataGridCliente.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridCliente.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridCliente.ColumnHeadersHeight = 30;
            this.dataGridCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridCliente.EnableHeadersVisualStyles = false;
            this.dataGridCliente.GridColor = System.Drawing.Color.RoyalBlue;
            this.dataGridCliente.Location = new System.Drawing.Point(16, 359);
            this.dataGridCliente.Name = "dataGridCliente";
            this.dataGridCliente.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DarkBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridCliente.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridCliente.RowHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DarkBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.CornflowerBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridCliente.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridCliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridCliente.Size = new System.Drawing.Size(750, 318);
            this.dataGridCliente.TabIndex = 19;
            // 
            // VistaCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkBlue;
            this.ClientSize = new System.Drawing.Size(800, 750);
            this.Controls.Add(this.dataGridCliente);
            this.Controls.Add(this.BotonEliminar);
            this.Controls.Add(this.BotonModificar);
            this.Controls.Add(this.PanelAgregarCliente);
            this.Controls.Add(this.PanelSupCliente);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VistaCliente";
            this.Text = "VistaCliente";
            this.Load += new System.EventHandler(this.VistaCliente_Load);
            this.PanelSupCliente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BotonMaximizarCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonRestaurarCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonMinimizarCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BotonCerrarCliente)).EndInit();
            this.PanelAgregarCliente.ResumeLayout(false);
            this.PanelAgregarCliente.PerformLayout();
            this.PaneEmpresa.ResumeLayout(false);
            this.PaneEmpresa.PerformLayout();
            this.PanelNatural.ResumeLayout(false);
            this.PanelNatural.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCliente)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelSupCliente;
        private System.Windows.Forms.PictureBox BotonCerrarCliente;
        private System.Windows.Forms.PictureBox BotonMinimizarCliente;
        private System.Windows.Forms.PictureBox BotonRestaurarCliente;
        private System.Windows.Forms.PictureBox BotonMaximizarCliente;
        private System.Windows.Forms.Panel PanelAgregarCliente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BotonModificar;
        private System.Windows.Forms.Button BotonEliminar;
        private System.Windows.Forms.DataGridView dataGridCliente;
        private System.Windows.Forms.ComboBox comboboxcomuna;
        private System.Windows.Forms.TextBox txttelefono;
        private System.Windows.Forms.TextBox txtmail;
        private System.Windows.Forms.TextBox txtnumerocalle;
        private System.Windows.Forms.TextBox txtcalle;
        private System.Windows.Forms.TextBox txtpassword;
        private System.Windows.Forms.TextBox txtuser;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox CheckBoxNaturalEmpresa;
        private System.Windows.Forms.Panel PaneEmpresa;
        private System.Windows.Forms.Panel PanelNatural;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button BotonGuardar_Persona;
        private System.Windows.Forms.DateTimePicker dateTime_persona;
        private System.Windows.Forms.TextBox txtapellido_persona;
        private System.Windows.Forms.TextBox txtnombre_persona;
        private System.Windows.Forms.TextBox txtrut_persona;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtrut_empresa;
        private System.Windows.Forms.Button BotonGuardar_Empresa;
        private System.Windows.Forms.TextBox txtrol_empresa;
        private System.Windows.Forms.TextBox txtnombre_empresa;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
    }
}