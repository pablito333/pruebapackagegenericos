﻿using Prueba1.Controlador.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Oracle.DataAccess.Client;

namespace Prueba1.Vista
{
    public partial class VistaProducto : Form
    {


        ProductoNegocio pro = new ProductoNegocio();
        private bool Modificar = false;
        private String idx = "";
        public VistaProducto()
        {
            InitializeComponent();
            CargarComboBoxProducto();
            CargarComboBoxProveedor();
            dataGridProducto.ReadOnly = true;
        }

        private void BotonCerrarProducto_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BotonMinimizarProducto_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }



        private void BotonMaximizarProducto_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            BotonMaximizarProducto.Visible = false;
            BotonRestaurarProduct.Visible = true;
        }

        private void BotonRestaurarProduct_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            BotonRestaurarProduct.Visible = false;
            BotonMaximizarProducto.Visible = true;
        }

        private void VistaProducto_Load(object sender, EventArgs e)
        {
            MostrarTabla();

        }
        private void CargarComboBoxProducto()
        {

            ConexionOracle con = new ConexionOracle();
            OracleCommand cmd = new OracleCommand();
            OracleDataAdapter objAdapter = new OracleDataAdapter();
            DataTable dt = new DataTable();

            cmd.Connection = con.GetOracleConnection();
            cmd.CommandText = "ps_genericos.cargar_tipo_producto";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("cursortipo_producto", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            objAdapter.SelectCommand = cmd;

            objAdapter.Fill(dt);
            comboProducto.DataSource = dt;
            comboProducto.DisplayMember = "DESC_PRODUCT";
            comboProducto.ValueMember = "ID_PRODUCT";


        }

        private void CargarComboBoxProveedor()
        {


            ConexionOracle con = new ConexionOracle();
            OracleCommand cmd = new OracleCommand();
            OracleDataAdapter objAdapter = new OracleDataAdapter();
            DataTable dt = new DataTable();

            cmd.Connection = con.GetOracleConnection();
            cmd.CommandText = "ps_genericos.cargar_proveedor";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("cursorproveedor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            objAdapter.SelectCommand = cmd;

            objAdapter.Fill(dt);
            comboProveedor.DataSource = dt;
            comboProveedor.DisplayMember = "nombre_proveedor";
            comboProveedor.ValueMember = "id_proveedor";

        }
        private void MostrarTabla()
        {
            ProductoNegocio producto = new ProductoNegocio();
            dataGridProducto.DataSource = producto.MostrarProduc();
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        private void PanelSupProducto_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void CheckBoxVencimiento_CheckedChanged(object sender, EventArgs e)
        {
            this.DateVencimiento.Visible = true;
            if (CheckBoxVencimiento.Checked == true)
            {
                this.DateVencimiento.Visible = true;
            }
            if (CheckBoxVencimiento.Checked == false)
            {
                this.DateVencimiento.Visible = false;
            }

        }

        private void BotonGuardar_Click(object sender, EventArgs e)
        {
                    if (Modificar == false)
                    {
                        try
                        {
                            String fktipoproducto = comboProducto.SelectedValue.ToString();
                            String fkproveedor = comboProveedor.SelectedValue.ToString();
                            String day = "11";
                            String month = "11";
                            String year = "1111";
                            if (CheckBoxVencimiento.Checked == true)
                            {
                                day = DateVencimiento.Value.Day.ToString();
                                month = DateVencimiento.Value.Month.ToString();
                                year = DateVencimiento.Value.Year.ToString();
                            }


                            ProductoNegocio pro = new ProductoNegocio();
                            pro.InsertarProducto(textProducto.Text, textPrecio.Text, textStock.Text, textStockCritico.Text, fkproveedor, fktipoproducto, day, month, year);
                            MostrarTabla();
                            Limpiar();

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error: " + ex);
                        }
                    }
                    if (Modificar == true)
                    {

                        try
                        {
                            String fktipoproducto = comboProducto.SelectedValue.ToString();
                            String fkproveedor = comboProveedor.SelectedValue.ToString();
                            String day = "11";
                            String month = "11";
                            String year = "1111";
                            if (CheckBoxVencimiento.Checked == true)
                            {
                                day = DateVencimiento.Value.Day.ToString();
                                month = DateVencimiento.Value.Month.ToString();
                                year = DateVencimiento.Value.Year.ToString();
                            }

                            ProductoNegocio pro = new ProductoNegocio();
                            pro.ModificarProducto(textProducto.Text, textPrecio.Text, textStock.Text, textStockCritico.Text, fkproveedor, fktipoproducto, day, month, year, idx);
                            MostrarTabla();
                            BotonGuardar.Text = "Guardar";
                            Modificar = false;
                            Limpiar();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error: " + ex);
                        }


                    }
                
            
        }





        private void BotonModificar_Click(object sender, EventArgs e)
        {
            if (dataGridProducto.SelectedRows.Count > 0)
            {

                textProducto.Text = dataGridProducto.CurrentRow.Cells["DESCRIPCION"].Value.ToString();
                textPrecio.Text = dataGridProducto.CurrentRow.Cells["PRECIO"].Value.ToString();
                textStock.Text = dataGridProducto.CurrentRow.Cells["STOCK"].Value.ToString();
                textStockCritico.Text = dataGridProducto.CurrentRow.Cells["STOCK_CRITICO"].Value.ToString();
                comboProducto.Text = dataGridProducto.CurrentRow.Cells["TIPO_PRODUCTO"].Value.ToString();
                comboProveedor.Text = dataGridProducto.CurrentRow.Cells["PROVEEDOR"].Value.ToString();
                idx = dataGridProducto.CurrentRow.Cells["ID_PRODUCTO"].Value.ToString();
                Modificar = true;
                BotonGuardar.Text = "Confirmar Modificacion";
            }
            else
            {

                MessageBox.Show("Seleccione Fila");
            }
        }
        private void Limpiar()
        {

            textProducto.Clear();
            textPrecio.Clear();
            textStock.Clear();
            textStockCritico.Clear();
            comboProducto.SelectedIndex = 0;
            comboProveedor.SelectedIndex = 0;
            DateVencimiento.Value = DateTime.Now;
        }

        private void BotonEliminar_Click(object sender, EventArgs e)
        {
            if (dataGridProducto.SelectedRows.Count > 0)
            {
                idx = dataGridProducto.CurrentRow.Cells["ID_PRODUCTO"].Value.ToString();
                pro.EliminarProducto(idx);
                MostrarTabla();
                Limpiar();

            }
            else
            {

                MessageBox.Show("Seleccione Fila");
            }
        }
       

        




    }
}
