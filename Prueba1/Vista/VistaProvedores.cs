﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Oracle.DataAccess.Client;
using Prueba1.Controlador.Negocio;

namespace Prueba1.Vista
{
    public partial class VistaProvedores : Form
    {
        private bool Modificar = false;
        private String idx = "";
        public VistaProvedores()
        {
            InitializeComponent();
            dataGridProveedor.ReadOnly = true;
        }

        private void BotonMinimizarCliente_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void BotonCerrarCliente_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BotonMaximizarCliente_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            BotonMaximizarProveedor.Visible = false;
            BotonRestaurarProveedor.Visible = true;
        }

        private void BotonRestaurarCliente_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            BotonRestaurarProveedor.Visible = false;
            BotonMaximizarProveedor.Visible = true;
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void PanelSupProveedores_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void MostrarTabla()
        {
            ProveedorNegocio proveedor = new ProveedorNegocio();
            dataGridProveedor.DataSource = proveedor.MostrarProveedor();
        }

        private void VistaProvedores_Load(object sender, EventArgs e)
        {
            MostrarTabla();
        }

        private void Limpiar()
        {
            txtnombre.Clear();
            txtfono.Clear();
            txtrubro.Clear();
            txtuser.Clear();
            txtpassword.Clear();
        }

        private void BotonGuardar_Proveedor_Click(object sender, EventArgs e)
        {
            if (Modificar == false)
            {
                try
                {

                    ProveedorNegocio prove = new ProveedorNegocio();
                    prove.InsertarProveedor(txtnombre.Text, txtfono.Text, txtrubro.Text, txtuser.Text, txtpassword.Text);
                    MostrarTabla();
                    Limpiar();

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex);
                }
            }
            if (Modificar == true)
            {

                    try
                    {
                        ProveedorNegocio prove = new ProveedorNegocio();
                        prove.ModificarProveedor(txtnombre.Text, txtfono.Text, txtrubro.Text, txtuser.Text, txtpassword.Text, idx);
                        MostrarTabla();
                        Limpiar();
                        BotonGuardar_Proveedor.Text = "Guardar";
                        Modificar = false;
                        Limpiar();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex);
                    }
             }
            
        }
        private void BotonModificar_Click(object sender, EventArgs e)
        {
            if (dataGridProveedor.SelectedRows.Count > 0)
            {

                txtnombre.Text = dataGridProveedor.CurrentRow.Cells["NOMBRE_PROVEEDOR"].Value.ToString();
                txtfono.Text = dataGridProveedor.CurrentRow.Cells["FONO"].Value.ToString();
                txtrubro.Text = dataGridProveedor.CurrentRow.Cells["RUBRO"].Value.ToString();
                txtuser.Text = dataGridProveedor.CurrentRow.Cells["USER_PROVEEDOR"].Value.ToString();
                txtpassword.Text = dataGridProveedor.CurrentRow.Cells["PASS_PROVEEDOR"].Value.ToString();
                idx = dataGridProveedor.CurrentRow.Cells["ID_PROVEEDOR"].Value.ToString();
                Modificar = true;
                BotonGuardar_Proveedor.Text = "Confirmar Modificacion";
            }
            else
            {

                MessageBox.Show("Seleccione Fila");
            }
        }

        private void BotonEliminar_Click(object sender, EventArgs e)
        {
            if (dataGridProveedor.SelectedRows.Count > 0)
            {
                idx = dataGridProveedor.CurrentRow.Cells["ID_PROVEEDOR"].Value.ToString();
                ProveedorNegocio proveedor = new ProveedorNegocio();
                proveedor.EliminarProveedor(idx);
                MostrarTabla();
                Limpiar();

            }
            else
            {

                MessageBox.Show("Seleccione Fila");
            }
        }
    }
}

